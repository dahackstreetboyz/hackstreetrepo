var freeplaymenu= function(game){
    
    this.MATHSTEROIDS_STATE="Mathsteroids";
    this.BASKETBALL_STATE="Basketball";
    this.CONVEYOR_STATE="Conveyor";
    this.DDR_STATE="Dance";
    this.MENU_STATE="MainMenu";

}

freeplaymenu.prototype = {
    
    preload: function() {
        //for the splash screen
        this.game.load.image('basketball_fp', 'assets/freeplaymenu/basketball_fp.png');
        this.game.load.image('ddr_fp', 'assets/freeplaymenu/ddr_fp.png');
        this.game.load.image('menu-button', 'assets/freeplaymenu/menu_button.png');

        //updateed
        this.game.load.image('fp-background', 'assets/freeplaymenu/fp_title_updated.png'); //background
        this.game.load.image('math_title', 'assets/freeplaymenu/math_title.png');
        this.game.load.image('conveyor_title', 'assets/freeplaymenu/conveyor_title.png');
        this.game.load.image('basketball_title', 'assets/freeplaymenu/basketball_title.png');
        this.game.load.image('dance_title', 'assets/freeplaymenu/dance_title.png');
        this.game.load.image('return_instr', 'assets/freeplaymenu/return_instr.png'); //spacebar to go back
        this.game.load.image('click_to_play', 'assets/freeplaymenu/click_to_play.png'); //click to play


    },
    create: function(){
            
            var backdrop = this.game.add.sprite(this.game.world.centerX+15, this.game.world.centerY-150, 'fp-background');
            backdrop.scale.setTo(1.1,1.1);
            backdrop.anchor.setTo(0.5,0.5);
         
            this.makeText();

    },
    
    update: function(){
    if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR))
    {
        this.goToMenu();
    }

    },
    
    //make the text corresponding to the buttons as well as the title
    makeText: function() {
        this.math_title = this.game.add.button(this.game.world.centerX-240, 325, 'math_title', this.goToMath, this, 2, 1, 0).scale.setTo(.7,.7);
        this.conveyor_title = this.game.add.button(this.game.world.centerX-180, 425, 'conveyor_title', this.goToConveyor, this, 2, 1, 0).scale.setTo(.7,.7);
        this.basketball_title = this.game.add.button(this.game.world.centerX-220, 525, 'basketball_title', this.goToBasketball, this, 2, 1, 0).scale.setTo(.7,.7);
        this.dance_title = this.game.add.button(this.game.world.centerX-120, 625, 'dance_title', this.goToDance, this, 2, 1, 0).scale.setTo(.7,.7);

        this.return_instr = this.game.add.sprite(20, 740, 'return_instr');
        this.return_instr.scale.setTo(.5,.5);
        this.return_instr.alpha = 0;
        //tween to flash it in and out
        this.game.add.tween(this.return_instr).to( { alpha: 1 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);

        this.click_to_play = this.game.add.sprite(this.game.world.centerX-330, 220, 'click_to_play');
        this.click_to_play.scale.setTo(.7,.7);
        this.click_to_play.alpha = 0;
        //tween to flash it in and out
        this.game.add.tween(this.click_to_play).to( { alpha: 1 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);

    },

    //ENTERING LAND OF REDUNDANT CODE (Phaser made me do it)
    
    goToMath: function() {
      this.game.state.start(this.MATHSTEROIDS_STATE, true, false);
    },
    
    goToConveyor: function() {
        this.game.freePlayMode = true;
      this.game.state.start(this.CONVEYOR_STATE, true, false);
    },
    
    goToBasketball: function() {
      this.game.state.start(this.BASKETBALL_STATE), true, false;
    },
    
    goToDance: function() {
      this.game.state.start(this.DDR_STATE, true, false);
    },
    
    goToMenu: function() {
    console.log("menu");
      this.game.state.start(this.MENU_STATE, true, false);
    },
}
    