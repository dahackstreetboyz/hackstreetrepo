var mathsteroids = function(game){
    mathsteroids.game = game;
    
    this.WIDTH = 800;
    this.HEIGHT = 800;
    
    
    //QUESTION AND ANSWER INPUT CONSTANTS
    this.question;
    this.correctAnswers;
    this.incorrectAnswers;
    this.nQuestions = 4;
    
    //GAME CONSTANTS
    this.START_TIME;                  //this is set by the create() and resetGame() functions.
    this.TARGET_DELAY = 1500;         //number of ms between each target spawn
    this.CURRENT_DIFFICULTY = 'easy';
    
    //GAME this.IABLES
    this.intro = true;        //game state flags
    this.win = false;
    this.lose = false;     
    this.score = 0;           //actual score
    this.combo = 0;           //score multiplier. this is updated by the updateCombo function
    this.streak = 0;          //number of sequential hits without either missing a right answer or hitting a wrong answer
    this.secondsLeft = 100;   //100 is a default, this is updated by the updateSecondsLeft function
    this.haveClicked = false; //Used to ensure there are two separate clicks when resetting game.
    
    //DIFFICULTY CONSTANTS
    //These are set in the setDifficulty function
    //Manually adding values here will not change gameplay.
    this.SCORE_NEEDED;
    this.TIME_LIMIT_MS;
    this.MISS_PENALTY;
    this.CORRECT_VALUE;
    this.INCORRECT_PENALTY;
    
    //ROCKET CONSTANTS
    this.ROCKET_FIRE_DELAY = 200;
    this.ROCKET_VELOCITY = 400;
    this.ROCKET_SPREAD = 100;
    this.MAX_ROCKETS = 20;
    
    //ROCKET this.IABLES
    this.nRocketsRemaining;
    
    //MOUSE ATTACK CONSTANTS
    //These are set in the changeWeapon function
    //Manually adding values here will not change gameplay.
    this.MOUSE_BULLET_GRAVITY;
    this.MOUSE_BULLET_VELOCITY;
    this.MOUSE_CAPACITY;      //max number of bullets that can be loaded (nBulletsRemaining this.)
    this.MOUSE_FIRE_DELAY;    //number of ms between each bullet fired
    this.MOUSE_SPREAD;        //spread in this game is the range of randomness on the X axis
    this.MOUSE_RECOIL;        //recoil in this game is the range of randomness on the Y axis
    this.MOUSE_RELOAD_TIME;   //number of ms to reload weapon
    this.MOUSE_PROJECTILE;
    
    //MOUSE ATTACK this.IABLES
    //These are helpers for the mouse attack functions.
    //Values assigned here are defaults, don't change these.
    this.nBulletsRemaining = 0;
    this.targetedX = 200;
    this.targetedY = 200;
    this.weaponName;
    this.lastFireTimestamp;
    this.lastTargetTimestamp;
    this.lastMouseReloadTimestamp;
    this.lastMouseBurstFireTimestamp;
    this.mouseDown = false;
    this.reloading = false;
    
    //WORLD OBJECTS AND GROUPS
    this.obstacleGroup;          //The only obstacle is the ground, but this supports an arbitrary number.
    this.player;                       
    this.targets;
    this.bullets;                //This group contains both bullets and rockets. There is no rocket group.
    this.diamonds;
    
    //INPUT this.S
    this.spacebar;
    this.cursors;
    this.wKey;
    this.aKey;
    this.sKey;
    this.dKey;
    this.oneKey;
    this.twoKey;
    this.threeKey;
    this.fourKey;
    
    //DISPLAY STRINGS
    this.controlString = "A and D - Move left and Right.\nClick to shoot\n1,2,3 Change weapons\nSpace to shoot rockets\n";
    this.winString = "You win!!!!\n You are a true hero or something.\nGame Over!";
    this.loseString = "Oh no!\nYou ran out of time\nGame Over!\n";
    this.scoreNeededString = "Score Needed: ";
    this.comboString = "Combo Multiplier: ";
    this.streakString = "Current Streak: ";
    this.rocketString = "Rockets left: ";
    this.reloadingString = "reloading";
    this.timeString = "";
    this.scoreString = "score: ";
    this.ammoString = " ammo: ";
    
    
    //DISPLAY OBJECTS
    this.scoreNeededDisplay;
    this.reloadingDisplay;
    this.questionDisplay;
    this.controlDisplay;
    this.rocketDisplay;
    this.streakDisplay;
    this.scoreDisplay;
    this.comboDisplay;
    this.ammoDisplay;
    this.loseDisplay;
    this.timeDisplay;
    this.winDisplay;
    
    //UI SPRITES
    this.vulcanSprite;
    this.autocannonSprite;
    this.artillerySprite;
    this.timeLeftSprite;
    this.vulcanAmmoArray = [];
    this.autocannonAmmoArray = [];
    this.artilleryAmmo;
    
    
    this.GROUND_SHIFT = 0;
    
    //ACHIEVEMENTS
    //There are 14 achievements currently hard coded to an array, corresponding to their position in a spritesheet.
    //0 - try harder - lose game on easy
    //1,2,3 - easy/med/hard victories
    //4,5,6 - 500 pt, 750 pt, 1000 pt on hard
    //7,8,9,10 - win using only artillery, autocannon, vulcan, rockets
    //11,12,13 - get a 5, 10, 20 streak
    this.achievements = [false,false,false,false,false,false,false,false,false,false,false,false,false,false];
    this.achievementStrings = [
        "Lose game on easy",
        "Win on easy",
        "Win on normal",
        "Win on hard",
        "Score 500 points on hard",
        "Score 750 points on hard",
        "Score 1000 points on hard",
        "Win using only artillery",
        "Win using only autocannon",
        "Win using only vulcan",
        "Win using only rockets",
        "Have a 5 streak!",
        "Have a 10 streak!",
        "Have a 20 streak!"];
    this.achievementSprite;  
    this.achievementSpriteArray = [];
    
    //ACHIEVEMENT HELPERS
    this.haveUsedAutocannon = false;
    this.haveUsedVulcan = false;
    this.haveUsedArtillery = false;
    this.haveUsedRockets = false;
    
    
    //TALENTS
    this.speedBoost = true;
    this.superjump = false;
    this.doubleJump = false;
    this.autocannonROF = true;
    this.autocannonGRAV = false;
    this.autocannonCAP = false;
    this.vulcanCAP = false;
    this.vulcanVEL = false;
    this.vulcanREL = false;
    this.artilleryGRAV = false;
    this.artilleryVEL = false;
    this.artilleryREL = false;
    this.rocketCAP = false;
    this.rocketSPREAD = false;
    this.infiniteRocket = false;
    
    //TALENT HELPERS
    this.haveDoubleJumped = false;
    this.DOUBLE_JUMP_DELAY = 1000;
    this.lastJumpTime;
    
    
    //CHARACTER CUSTOMIZATION this.S
    this.heads = ["characters/head0","characters/head1","characters/head2"];
    this.bodies = ["characters/head0","characters/head1","characters/head2"];
    this.legs = ["characters/head0","characters/head1","characters/head2"];
    this.headNumber;
    this.bodyNumber;
    this.legNumber;
    
    
    //TUTORIAL this.S
    this.tutorialPage = 0;
    this.tutorialSprites = [];


    //MUSIC VARS
    this.music;


}






mathsteroids.prototype = {
    
    
    preload: function(){
        
        
    },
    
    create: function(){
              
        //INITIALIZE PHYSICS
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        
        
        //INITIALIZE INPUT
    	this.cursors = this.game.input.keyboard.createCursorKeys();
    	this.spacebar = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    	this.wKey = this.game.input.keyboard.addKey(Phaser.Keyboard.W);
        this.aKey = this.game.input.keyboard.addKey(Phaser.Keyboard.A);
        this.sKey = this.game.input.keyboard.addKey(Phaser.Keyboard.S);
        this.dKey = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
        this.oneKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ONE);
        this.twoKey = this.game.input.keyboard.addKey(Phaser.Keyboard.TWO);
        this.threeKey = this.game.input.keyboard.addKey(Phaser.Keyboard.THREE);
        this.fourKey = this.game.input.keyboard.addKey(Phaser.Keyboard.FOUR);
         
         
         
        //INITIALIZE VARIABLES AND CONSTANTS
    	this.nRocketsRemaining = this.MAX_ROCKETS;
        this.secondsLeft = this.updateSecondsLeft();
        this.setDifficulty(this.CURRENT_DIFFICULTY);
        
        
        //INITIALIZE TIMESTAMPS
    	this.lastTargetTimestamp = this.game.time.now;
    	this.lastFireTimestamp = this.game.time.now;
    	this.lastMouseReloadTimestamp = this.game.time.now;
    	this.lastMouseBurstFireTimestamp = this.game.time.now;
    	
    	
    	
        //CREATE BACKGROUND
        this.game.add.sprite(0, 35, 'sky');
        this.game.add.sprite(0, 0, 'top');
     //   this.game.add.text(10, 10, this.game.testVar, { font: '34px Arial', fill: '#fff' });
        this.game.add.sprite(0,this.HEIGHT-96-this.GROUND_SHIFT,this.game.playerTexture);
        //game.add.sprite(0, 200, 'sky');
     
        
        //CREATE GROUND
        this.obstacleGroup = this.game.add.group();
        this.obstacleGroup.enableBody = true;
        var ground = this.obstacleGroup.create(0, this.game.world.height - 64-this.GROUND_SHIFT, 'ground');
        ground.scale.setTo(2, 2);
        ground.body.immovable = true;
     
        //CREATE PLAYER
        this.player = this.game.add.sprite(32, this.game.world.height - 150-this.GROUND_SHIFT, 'dude');
        this.game.physics.arcade.enable(this.player);
        this.player.body.bounce.y = 0.2;
        this.player.body.gravity.y = 300;
        this.player.body.collideWorldBounds = true;
        this.player.animations.add('left', [0, 1, 2, 3], 10, true);
        this.player.animations.add('right', [5, 6, 7, 8], 10, true);
        
        //CREATE EMPTY GROUP FOR TARGETS
    	this.targets = this.game.add.group();
    	this.targets.enableBody = true;
    	
    	//CREATE EMPTY GROUP FOR DIAMONDS
    	this.diamonds = this.game.add.group();
    	this.diamonds.enableBody = true;
        this.diamonds.setAll('outOfBoundsKill', true);    //targets self-destruct when going off screen.
        this.diamonds.setAll('checkWorldBounds', true);
     	
    	//CREATE EMPTY GROUP FOR BULLETS AND ROCKETS
    	this.bullets = this.game.add.group();
    	this.bullets.enableBody=true;
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);
        
    	//CREATE THE FIRST TARGET
    	this.createTarget();
    	
    	
    	//SEPARATE ACHIEVEMENT SPRITE SHEET INTO SEPARATE SPRITES
    	for(var i = 0; i < 14; i++){
    	    var thisAchiev =  this.game.add.sprite(100+32*i,100-32,'achievementSprite');
    	    this.achievementSpriteArray[i] = thisAchiev;
    	    thisAchiev.animations.add('show',[i],10,true);
    	    thisAchiev.animations.play('show');
    	    thisAchiev.visible = false;
    	}
    	
    	
    	//CREATE AMMO SPRITES
    	
    	var vulcanLeftOffset = 100;
    	var vulcanBottomOffset = 5+32;
    	var vulcanSpacing = 9;
    	var maxVulcanCap = 30;
    	for(var i = 0; i < maxVulcanCap; i++){
    	    this.vulcanAmmoArray[i] = this.game.add.sprite(vulcanLeftOffset+vulcanSpacing*i,this.game.world.height-vulcanBottomOffset,'vulcanShell');
    	    this.vulcanAmmoArray[i].visible = false;
    	}
    	 
    	var cannonLeftOffset = 140;
    	var cannonBottomOffset = 5+64;
    	var cannonSpacing = 40;
    	var maxcannonCap = 4;
    	for(var i = 0; i < maxcannonCap; i++){
    	    this.autocannonAmmoArray[i] = this.game.add.sprite(cannonLeftOffset+cannonSpacing*i,this.game.world.height-cannonBottomOffset,'autocannonShell');
    	    this.autocannonAmmoArray[i].visible = false;
    	}
    	 
    	var artilleryLeftOffset = 140;
    	var artilleryBottomOffset = 5+128;
    	
    	this.artilleryAmmo = this.game.add.sprite(artilleryLeftOffset, this.game.world.height - artilleryBottomOffset, 'artilleryShell');
    	this.artilleryAmmo.visible = false;
    	 
    	 
    	//INITIALIZE TUTORIAL PAGES
    /*	this.tutorialSprites[0] = this.game.add.sprite(0, 0, 't0');
    	this.tutorialSprites[1] = this.game.add.sprite(0, 0, 't1');
    	this.tutorialSprites[2] = this.game.add.sprite(0, 0, 't2');
    	this.tutorialSprites[3] = this.game.add.sprite(0, 0, 't3');
    	this.tutorialSprites[4] = this.game.add.sprite(0, 0, 't4');
    	this.tutorialSprites[5] = this.game.add.sprite(0, 0, 't5');
    	this.tutorialSprites[6] = this.game.add.sprite(0, 0, 't6');
    	this.tutorialSprites[7] = this.game.add.sprite(0, 0, 't7');
    	this.tutorialSprites[8] = this.game.add.sprite(0, 0, 't8');
    	this.tutorialSprites[9] = this.game.add.sprite(0, 0, 't9');
    	this.tutorialSprites[10] = this.game.add.sprite(0, 0, 't10');
    	this.tutorialSprites[11] = this.game.add.sprite(0, 0, 't11');
    	this.tutorialSprites[12] = this.game.add.sprite(0, 0, 't12');
    	this.tutorialSprites[13] = this.game.add.sprite(0, 0, 't13');
    	this.tutorialSprites[14] = this.game.add.sprite(0, 0, 't14');
    	this.tutorialSprites[15] = this.game.add.sprite(0, 0, 't15');
    	this.tutorialSprites[16] = this.game.add.sprite(0, 0, 't16');
    	this.tutorialSprites[17] = this.game.add.sprite(0, 0, 't17');
    	this.tutorialSprites[18] = this.game.add.sprite(0, 0, 't18');
    	for(var i = 0; i < 19; i++){
    	    this.tutorialSprites[i].visible= false;
    	}
    	*/
    	
    	this.tutorialSprites[0] = this.game.add.sprite(0,0,'instructions');
    	this.tutorialSprites[1] = this.game.add.sprite(0,0,'controls');
    	this.tutorialSprites[0].visible = false;
    	this.tutorialSprites[1].visible = false;
    	 
    	//INITIALIZE DISPLAYS
    	this.scoreDisplay = this.game.add.text(10, 10, this.scoreString + this.score, { font: '34px Arial', fill: '#fff' });
        this.questionDisplay = this.game.add.text(250, 10, this.question, { font: '34px Arial', fill: '#fff' });
        this.reloadingDisplay = this.game.add.text(300,this.game.world.height-50-this.GROUND_SHIFT,this.reloadingString, {font: '20px Arial',fill: '#fff'});
        this.ammoDisplay = this.game.add.text(10, this.game.world.height-60-this.GROUND_SHIFT, this.ammoString + this.nBulletsRemaining, {font: '20px Arial', fill: '#fff'});
        this.rocketDisplay = this.game.add.text(15,this.game.world.height-20-this.GROUND_SHIFT, this.rocketString + this.nRocketsRemaining, {font: '12px Arial', fill: '#fff'});
      //  this.controlDisplay = this.game.add.text(600,this.game.world.height-60-this.GROUND_SHIFT,this.controlString, {font: '12px Arial',fill:'#fff'});
        this.winDisplay = this.game.add.text(100,200,this.winString,{font: '34px Arial',fill:'#fff'});
        this.winDisplay.visible = false;
        this.loseDisplay = this.game.add.text(100,200,this.oseString,{font: '34px Arial',fill:'#fff'});
        this.loseDisplay.visible = false;
        this.timeDisplay = this.game.add.text(680,65,this.timeString,{font: '32px Arial',fill:'#fff'});
        this.scoreNeededDisplay = this.game.add.text(650,10,this.scoreNeededString + this.SCORE_NEEDED,{font: '12px Arial',fill:'#fff'});
        this.comboDisplay = this.game.add.text(10, 50, this.comboString + this.combo, { font: '12px Arial', fill: '#fff' });
        this.streakDisplay =  this.game.add.text(10, 40, this.streakString + this.streak, { font: '12px Arial', fill: '#fff' });
   
        //INITIALIZE UI SPRITES
        this.vulcanSprite = this.game.add.sprite(15,this.game.world.height-35, 'vulcanSprite');
        this.autocannonSprite = this.game.add.sprite(15,this.game.world.height-35, 'autocannonSprite');
        this.artillerySprite = this.game.add.sprite(15,this.game.world.height-35, 'artillerySprite');
        this.vulcanSprite.visible = false;
        this.autocannonSprite.visible = false;
        this.artillerySprite.visible = false;
        
        this.timeLeftSprite = this.game.add.sprite(625,30,'timeLeftSprite');
   
        //Load first question
        this.loadQuestion();
        
        //initialize music
        this.game.music.stop();
        this.game.music = this.game.add.audio('mathsteroidsTheme');
        this.game.music.loop = true;
        this.game.music.play();
        
        
    	this.changeWeapon('autocannon'); 
    },
    
    update: function(){
        
      
        
       
        this.updatePhysics();
        
        //Keep score non-negative
        if(this.score<0)
            this.score = 0;
        
        //Check win and loss conditions, update flags.
        if(this.score > this.SCORE_NEEDED && this.secondsLeft <= 0)
            this.win = true;
        else if(this.secondsLeft <= 0)
            this.lose = true;
        
        //Perform the correct loop function according to state
        if(this.intro == true){
            this.introScreen();
        }
        else if(this.win == true){
           // this.game.music.loop=false;
            //this.game.music.stop();
            if(this.game.freeUnlocked == false){
                this.game.state.start("MathsteroidsEnding");
            }
            else{
                
                this.gameOverWinScreen();
            }
        }
        else if(this.lose == true){
            this.gameOverLossScreen();
        }
        else{
            this.gameplayLoop();
        }
        
        
    },
    
    
//
//GAME LOGIC FUNCTIONS
//

    introScreen: function(){
        this.hideUI();
        
        var nTutorialSprites = 2; //change to 19 if using old model
        if(this.tutorialPage < nTutorialSprites){
            this.tutorialSprites[this.tutorialPage].visible = true;
            if(this.tutorialPage == nTutorialSprites){
                this.questionDisplay.visible = true;
            }
        }
        else{
                for(var i = 0; i < nTutorialSprites; i++){
                    this.tutorialSprites[i].visible = false;
                    this.tutorialSprites[i].kill();
                }
                this.hideAchievements();
                this.showUI();
                this.intro = false; 
                this.START_TIME =this.game.time.now;
        }
        
        if(this.game.input.activePointer.isDown && this.haveClicked == false){
            
                this.tutorialPage++;
                this.haveClicked = true;
        }
        else if(this.spacebar.isDown){
            this.tutorialPage = 20;
        }
        else if(this.game.input.activePointer.isUp){ //used to debounce clicking when resetting thethis.game
            this.haveClicked = false;
        }
    },
    
    gameOverLossScreen: function(){
        this.loseDisplay.visible = true;
        this.updateAchievements();
        this.showAchievements();
        if(this.game.input.activePointer.isDown){
            if(this.CURRENT_DIFFICULTY == 'hard'){
                this.resetGame('medium');
            }
            else{
                this.resetGame('easy');
            }
            this.haveClicked = true;
        }
    },
    
    gameOverWinScreen: function(){
        this.winDisplay.visible = true;
        this.updateAchievements();
        this.showAchievements();
            if(this.game.input.activePointer.isDown){
                if(this.CURRENT_DIFFICULTY == 'easy')
                {
                    this.resetGame('medium');
                }
                else 
                {
                    this.resetGame('hard');
                }
                this.haveClicked = true;
            }
    },
    
    gameplayLoop: function(){
            
            //ALL USER INPUT IS DEFINED IN THE FUNCTION DIRECTLY BELOW THIS
            this.userInputScanner();
            
            //CREATE A NEW TARGET. 
            //Note that the createTarget function handles the interval delay checking.
            this.createTarget();
            
            //Update the combo multiplier
            this.updateCombo();
            
            //Update all of the text on screen
            this.updateDisplays();
            
            this.updateAchievements();
            
            if(this.mouseDown)   //mouseDown is a flag set in the userInputScanner
                this.fire();     
            
            //Keep the reload timer running.
            this.continueReload();
            
    },
    
    //This function polls user input and responds accordingly.
    //This is only used in the main gameplay loop. User input for other game states is defined manually elsewhere.
    userInputScanner: function(){
            //PLAYER X MOVEMENT
            if (this.cursors.left.isDown || this.aKey.isDown)
            {
                if(this.speedBoost){
                    this.player.body.velocity.x = -500;
                }
                else{
                    this.player.body.velocity.x = -250;
                }
                this.player.animations.play('left');
            }
            else if (this.cursors.right.isDown || this.dKey.isDown)
            {
                if(this.speedBoost){
                    this.player.body.velocity.x = 500;
                }
                else{
                    this.player.body.velocity.x = 250;
                }
                this.player.animations.play('right');
            }
            else
            {
                this.player.animations.stop();
                this.player.frame = 4;
            }
            
            //PLAYER Y MOVEMENT
            if((this.cursors.up.isDown || this.wKey.isDown ) && this.player.body.touching.down)
            {
                if(this.superjump){
                    this.player.body.velocity.y = -400;
                }
                else{
                    this.player.body.velocity.y = -200;
                }
                this.haveDoubleJumped = false;
                this.lastJumpTime = this.game.time.now;
            }
            else if((this.cursors.up.isDown || this.wKey.isDown ) 
                        && this.doubleJump == true && this.haveDoubleJumped == false
                        && this.game.time.now > this.lastJumpTime + this.DOUBLE_JUMP_DELAY){
                this.player.body.velocity.y = -400;
                this.haveDoubleJumped = true;
            }
            else if(this.cursors.down.isDown || this.sKey.isDown)
            {
                this.player.body.velocity.y = 200;
            }
            
            //BULLET AIMING/FIRING
            if(this.game.input.activePointer.isDown){
                this.mouseDown = true;            
                this.aim(this.game.input.activePointer.x, this.game.input.activePointer.y);
            }
            else{
                this.mouseDown = false;
            }
            
            //ROCKET FIRING
            if(this.spacebar.isDown)
            {
           	    //this.launchRocket();
           	//    mouseDown = true;
              //  this.aim(this.game.input.activePointer.x, this.game.input.activePointer.y);
           	    
            }
            else{
               // mouseDown = false;
            }
            
            //WEAPON SELECTION
            if(this.oneKey.isDown){
                this.changeWeapon('vulcan');
            }
            else if(this.twoKey.isDown){
                this.changeWeapon('autocannon');
            }
            else if(this.threeKey.isDown){
                this.changeWeapon('artillery');
            }
    },
    
//
//WEAPON FUNCTIONS
//

    //This function launches a rocket in the air.
    //Rockets have constant variation on the X axis, and are always "aimed" directly upwards.
    launchRocket: function(){
    	if(this.game.time.now > this.lastFireTimestamp + this.ROCKET_FIRE_DELAY  && this.nRocketsRemaining>0){
    		
    	    var bullet = this.bullets.create(this.player.x+10, this.player.y, 'bullet');
    	    bullet.body.velocity.y = -this.ROCKET_VELOCITY;
    	    bullet.body.velocity.x = (Math.random()*this.ROCKET_SPREAD)-this.ROCKET_SPREAD/2;
    	    this.lastFireTimestamp = this.game.time.now;
    	  //  nRocketsRemaining--;
    	    this.haveUsedRockets = true;
    	}
        
    },
    
    
    //This function updates the location that bullets are sent towards
    //It is called whenever the left mouse button is pressed
    aim: function(x,y){
        
        this.targetedX = x;
        this.targetedY = y;
    },
    
    //This function continues firing and keeps track of ammunition count/reloading.
    //It is called by update within an if block which checks if the mouse is down
    fire: function(){
        
        if(this.nBulletsRemaining > 0 && this.game.time.now > (this.lastMouseBurstFireTimestamp + this.MOUSE_FIRE_DELAY)){
            this.shootAt(this.targetedX, this.targetedY);
            this.nBulletsRemaining--;
            this.lastMouseBurstFireTimestamp = this.game.time.now;
            
            
            if(this.weaponName == "Artillery"){
                //console.log("artillery ammo invisible");
                this.artilleryAmmo.visible = false;
            }
            else if(this.weaponName == "Autocannon"){
               //console.log("autocannon ammo array " + this.nBulletsRemaining);
                this.autocannonAmmoArray[this.nBulletsRemaining].visible = false;
            }
            else if(this.weaponName == "Vulcan Cannon"){
               // console.log("vulcan ammo array " + this.nBulletsRemaining);
                this.vulcanAmmoArray[this.nBulletsRemaining].visible = false;
            }
            
            
        }
        else if(this.nBulletsRemaining == 0 && this.reloading == false){
            this.reloading = true;
            this.lastMouseReloadTimestamp = this.game.time.now;
        }
    },
    
    //This function sends a bullet to the coordinates x,y
    //it adds some randomness from the recoil and spread constants for the weapon.
    //Note that bullets are also affected by gravity.
    //This is called only by the fire function.
    shootAt: function(x,y){
        
            var recoil = Math.random()*this.MOUSE_RECOIL-this.MOUSE_RECOIL/2;
            var spread = Math.random()*this.MOUSE_SPREAD-this.MOUSE_SPREAD/2;
            x+= spread;
            y+= recoil;
            var yDist = this.player.y - y;
            var xDist;
            var bullet = this.bullets.create(this.player.x+10, this.player.y, this.MOUSE_PROJECTILE);
            if(x < this.player.x){ //to left of player
                xDist = this.player.x - x;
        	    var theta = (Math.atan(yDist/xDist));
        	    bullet.body.velocity.y = - (this.MOUSE_BULLET_VELOCITY * Math.sin(theta));
        	    bullet.body.velocity.x = - (this.MOUSE_BULLET_VELOCITY * Math.cos(theta));
            }
            else if(x> this.player.x){ //to right of player
                xDist = x - this.player.x;
        	    var theta = (Math.atan(yDist/xDist));
        	    bullet.body.velocity.y = - (this.MOUSE_BULLET_VELOCITY * Math.sin(theta));
        	    bullet.body.velocity.x = + (this.MOUSE_BULLET_VELOCITY * Math.cos(theta));
            }
            else{
                bullet.body.velocity.y = -this.MOUSE_BULLET_VELOCITY;
            }
            bullet.body.gravity.y = this.MOUSE_BULLET_GRAVITY;
    
            if(this.weaponName == 'Vulcan Cannon'){
                this.haveUsedVulcan = true;
            }
            else if(this.weaponName == 'Autocannon'){
                this.haveUsedAutocannon = true;
            }
            else if(this.weaponName == 'Artillery'){
                this.haveUsedArtillery = true;
            }
        
        
    },
    
    //This function makes reload checks continue even when the mouse isn't held.
    continueReload: function(){
        if(this.reloading ==true && this.game.time.now > this.lastMouseReloadTimestamp + this.MOUSE_RELOAD_TIME) {
            this.nBulletsRemaining = this.MOUSE_CAPACITY;
            this.reloading = false;
            
            
            if(this.weaponName == "Artillery"){
                this.artilleryAmmo.visible = true;
            }
            else if(this.weaponName == "Autocannon"){
                
                for(var i = 0; i < this.MOUSE_CAPACITY; i++){
                    this.autocannonAmmoArray[i].visible = true;
                }
                
            }
            else if(this.weaponName == "Vulcan Cannon"){
                for(var i = 0; i < this.MOUSE_CAPACITY; i++){
                    this.vulcanAmmoArray[i].visible = true;
                }
            }
        }
    },
    
   

//
//SPRITE CREATION FUNCTIONS
//

    //This function creates a new target at a random X location along the top edge of the window
    //Each target has an answer attached to it, and a boolean checked by the hit function to tell if it was correct.
    //Answers are stored in two separate arrays defined at the very top of this file.
    createTarget: function(){
    	
    	if(this.game.time.now >  this.lastTargetTimestamp+this.TARGET_DELAY){
    		
    	     var x = (Math.random()*(this.WIDTH-32));
       		 var target = this.targets.create(x, 100, 'UFOsprite');
 
       		 var display;
     		 
     		 if(Math.random() < 0.6){
     		 	target.isCorrect = true;
     		 	target.value = this.correctAnswers[Math.floor(Math.random()*this.correctAnswers.length)];
     		 	display = this.game.add.text(0,-20,target.value, {font: '24px Arial', fill: '#fff' });
     		 	target.addChild(display);
     		 }
     		 else{
     		 	target.isCorrect = false;
     		 	target.value = this.incorrectAnswers[Math.floor(Math.random()*this.incorrectAnswers.length)];
     		 	display = this.game.add.text(0,-20,target.value, {font: '24px Arial', fill: '#fff' });
     		 	target.addChild(display);
     		 }
     		 
        	 target.body.gravity.y = 20;
        	 target.hasPenalized = false;
        	 this.lastTargetTimestamp = this.game.time.now;
    	}
     
    },
    
    //This function creates a diamond that flies intelligently
    //towards the score in the top left corner.
    //It is called when a correct answer hits a bullet.
    addReward: function(target){
        
            var diamondSpeed = 500;
    	    var diamond = this.diamonds.create(target.x, target.y, 'diamond');
    	    var theta = (Math.atan(target.y/target.x));
    	    diamond.body.velocity.y = - (diamondSpeed * Math.sin(theta));
    	    diamond.body.velocity.x = - (diamondSpeed * Math.cos(theta));
    },
    
    //This function creates a diamond that falls downwards, off screen
    //It is called when a correct answer hits the ground.
    addLostReward: function(target){
            var diamond = this.diamonds.create(target.x,target.y, 'alienSprite');
            diamond.animations.add('run',[0,1,2,3],10,true);
            diamond.animations.play('run');
            diamond.scale.y = 0.5;
            diamond.scale.x = 0.5;
            diamond.body.velocity.y = 100;
    },

//
//SPRITE COLLISION FUNCTIONS
//

    hit: function(bullet,target){
    	if(target.isCorrect == true){
    	    this.streak++;
    		this.score+= this.CORRECT_VALUE * this.combo;
    		this.addReward(target);
        	target.kill();
    	}
    	else{
    	    if(target.hasPenalized == false){
    	        target.hasPenalized = true;
    		    this.score-=this.INCORRECT_PENALTY;  //penalty for hitting wrong values, to prevent just shooting all
    		    target.body.gravity.y = 600;
    	    }
    	    this.streak = 0;
    	}
    	bullet.kill();
    },
    
    miss: function(bullet,obstacle){
        bullet.kill();
    },
    
    targetHitsObstacle: function(target,obstacle){   
    	if(target.isCorrect == true){    
    	    this.streak = 0;
    		this.score-=this.MISS_PENALTY;
    		this.addLostReward(target);
    	}
    	target.kill();
    },
    
    
//
//HELPER FUNCTIONS
//DONT MESS WITH THESE
//
    updatePhysics: function(){
        this.game.physics.arcade.collide(this.player, this.obstacleGroup);
    	this.game.physics.arcade.overlap(this.bullets,this.targets, this.hit, null,this);
    	this.game.physics.arcade.overlap(this.bullets,this.obstacleGroup, this.miss,null,this);
        this.game.physics.arcade.overlap(this.targets, this.obstacleGroup, this.targetHitsObstacle, null, this);
        
        this.player.body.velocity.x = 0;
    },

    updateSecondsLeft: function(){
        var endTime = this.START_TIME + this.TIME_LIMIT_MS;
        this.secondsLeft = Math.floor((endTime - this.game.time.now) / 1000);
    },
    
    updateDisplays: function(){
        this.updateSecondsLeft();
        this.scoreDisplay.text = this.scoreString + this.score;
        this.ammoDisplay.text = this.weaponName;
        this.scoreNeededDisplay.text = this.scoreNeededString + this.SCORE_NEEDED;
        this.timeDisplay.text = this.timeString + this.secondsLeft;
        this.rocketDisplay.text = " ";
        this.comboDisplay.text = this.comboString + this.combo;
        this.streakDisplay.text = this.streakString + this.streak;
        if(this.reloading == true){
            this.reloadingDisplay.visible = true;
        }
        else{
            this.reloadingDisplay.visible = false;
        }
    },
    
    updateAchievements: function(){
        
        if(this.win == true){
            
                //ARTILLERY ONLY
                if(this.haveUsedVulcan == false && this.haveUsedAutocannon == false && this.haveUsedRockets== false){
                    this.grantAchievement(7);
                    this.artilleryGRAV = true;
                    this.artilleryVEL = true;
                }
                //AUTOCANNON ONLY
                else if(this.haveUsedVulcan== false && this.haveUsedArtillery== false && this.haveUsedRockets== false){
                    this.grantAchievement(8);
                    this.autocannonGRAV = true;
                    this.autocannonCAP = true;
                }
                //VULCAN ONLY
                else if(this.haveUsedArtillery== false && this.haveUsedAutocannon== false && this.haveUsedRockets== false){
                    this.grantAchievement(9);
                    this.vulcanVEL = true;
                    this.vulcanCAP = true;
                }
                //ROCKET ONLY
                else if(this.haveUsedVulcan== false && this.haveUsedAutocannon== false && this.haveUsedArtillery== false){
                    this.grantAchievement(10);
                    this.rocketCAP = true;
                    this.rocketSPREAD = true;
                }
            
            if(this.CURRENT_DIFFICULTY == 'easy'){
                this.grantAchievement(1);
            }
            else if(this.CURRENT_DIFFICULTY == 'medium'){
                this.grantAchievement(2);
            }
            else if(this.CURRENT_DIFFICULTY == 'hard'){
                
                this.grantAchievement(3);
                
                if(this.score >= 500){
                    this.grantAchievement(4);
                    this.speedBoost = true;
                }
                if(this.score >= 750){
                    this.grantAchievement(5);
                    this.autocannonROF = true;
                    this.vulcanREL = true;
                    this.artilleryREL = true;
                    this.doubleJump = true;
                    
                }
                if(this.score >= 1000){
                    this.grantAchievement(6);
                    this.superjump = true;
                    this.infiniteRocket = true;
                }
                
            }
        }
        else if(this.lose == true){
            if(this.CURRENT_DIFFICULTY == 'easy'){
                this.grantAchievement(0);
            }
        }
        else{
            if(this.streak>=5){
                this.grantAchievement(11);
            }
            if(this.streak>=10){
                this.grantAchievement(12);
            }
            if(this.streak>=20){
                this.grantAchievement(13);
            }
        }
        
    },
    
    showAchievements: function(){
        for(var i = 0; i < 14; i++){
            if(this.achievements[i] == true){
                this.achievementSpriteArray[i].visible=true;
            }
            else{
                this.achievementSpriteArray[i].visible=false;
            }
        }
    },
    
    hideAchievements: function(){
        for(var i = 0; i < 14; i++){
            this.achievementSpriteArray[i].visible=false;
        }
    },
    
    hideUI: function(){
        
        this.scoreDisplay.visible = false;
        this.questionDisplay.visible = false;
        this.reloadingDisplay.visible = false;
        this.ammoDisplay.visible = false;
        this.rocketDisplay.visible = false;
       // this.controlDisplay.visible = false;
        this.timeDisplay.visible = false;
        this.scoreNeededDisplay.visible = false;
        this.comboDisplay.visible = false;
        this.streakDisplay.visible = false;
        this.artillerySprite.visible = false;
        this.autocannonSprite.visible = false;
        this.vulcanSprite.visible = false;
        this.timeLeftSprite.visible = false;
    },
    
    showUI: function(){
        this.scoreDisplay.visible = true;
        this.questionDisplay.visible = true;
        this.reloadingDisplay.visible =true;
        this.ammoDisplay.visible = true;
        this.rocketDisplay.visible = true;
        //this.controlDisplay.visible = true;
        this.timeDisplay.visible =true;
        this.scoreNeededDisplay.visible = true;
        this.comboDisplay.visible = true;
        this.streakDisplay.visible = true;
        this.autocannonSprite.visible = true;
        this.timeLeftSprite.visible = true;
    },
    
    grantAchievement: function(number){
        this.achievements[number] = true;
    },
    
    resetAchievements: function(){
        for(var i = 0; i < 14; i++){
            this.achievements[i] = false;
        }
        this.speedBoost = false;
        this.superjump = false;
        this.doubleJump = false;
        this.autocannonCAP = false;
        this.autocannonGRAV = false;
        this.autocannonROF = false;
        this.vulcanCAP = false;
        this.vulcanVEL = false;
        this.vulcanREL = false;
        this.artilleryGRAV = false;
        this.artilleryVEL = false;
        this.artilleryREL = false;
        this.rocketCAP = false;
        this.rocketSPREAD = false;
        this.infiniteRocket = false;
    },
    
    resetAchievementHelpers: function(){
        this.haveUsedVulcan = false;
        this.haveUsedAutocannon = false;
        this.haveUsedArtillery = false;
        this.haveUsedRockets = false;
    },
    
    resetGame: function(newDifficulty){
        
        //SAVE STATS TO GAME GLOBAL VARS
        this.game.overallScore += this.score;
        this.game.mathsteroidsScore = this.score;
        this.game.mathsteroidsDifficulty = newDifficulty;
        
        //RESET TIMESTAMPS
        this.lastTargetTimestamp =this.game.time.now;
    	this.lastFireTimestamp =this.game.time.now;
    	this.lastMouseReloadTimestamp =this.game.time.now;
    	this.lastMouseBurstFireTimestamp =this.game.time.now;
    	
    	//RESET GAME STATE
    	this.score = 0;
    	this.streak = 0;
    	this.win=false;
    	this.lose=false;
    	this.intro = true;
    	this.tutorialPage = 18;
    	this.winDisplay.visible = false;
    	this.loseDisplay.visible = false;
    
    	//RESET WEAPONS
    	this.reloading=false;
    	this.nBulletsRemaining = 0;
    	this.nRocketsRemaining = this.MAX_ROCKETS;
    	this.resetAchievementHelpers();
    	
    	//UPDATE DIFFICULTY CONSTANTS
    	this.setDifficulty(newDifficulty);
    	
    	//USED TO GET PAST FIRST IF CHECK IN UPDATE
    	this.secondsLeft = 99999;
    	
    	this.loadQuestion();
    },


//
//VALUE SETTING FUNCTIONS
//
//CHANGE NUMBERS DOWN HERE TO CHANGE GAME BEHAVIOR
//


    //This magic-number filled function sets the thresholds that
    //tie combo multipliers to streak values
    updateCombo: function(){
        if(this.streak > 50){
            this.combo = 10;
        }
        else if(this.streak > 20){
            this.combo = 7;
        }
        else if(this.streak > 10){
            this.combo = 5;
        }
        else if(this.streak > 5){
            this.combo = 3;
        }
        else if(this.streak > 3){
            this.combo = 2;
        }
        else{
            this.combo = 1;
        }
    },
    
    setDifficulty: function(difficulty){
        
        this.CURRENT_DIFFICULTY = difficulty;
        
        if(difficulty == 'easy'){
            this.SCORE_NEEDED = 100;
            this.TIME_LIMIT_MS = 60000;
            this.CORRECT_VALUE = 10;
            this.INCORRECT_PENALTY = 5;
            this.MISS_PENALTY = 0;
        }
        else if(difficulty == 'medium'){
            this.SCORE_NEEDED = 200;
            this.TIME_LIMIT_MS = 60000;
            this.CORRECT_VALUE = 10;
            this.INCORRECT_PENALTY = 10;
            this.MISS_PENALTY = 5;
        }
        else if(difficulty == 'hard'){
            this.SCORE_NEEDED = 300;
            this.TIME_LIMIT_MS = 60000;
            this.CORRECT_VALUE = 10;
            this.INCORRECT_PENALTY = 20;
            this.MISS_PENALTY = 10;
        }
        
    },
    
    changeWeapon: function(name){
        this.nBulletsRemaining = 0;
        this.reloading = true;
        if(name == 'autocannon'){
            this.MOUSE_PROJECTILE = 'cannonball';
            this.MOUSE_BULLET_VELOCITY = 300;
            this.MOUSE_SPREAD = 25;
            this.MOUSE_RECOIL = 20;
            this.MOUSE_RELOAD_TIME = 900;
            if(this.autocannonGRAV){
                this.MOUSE_BULLET_GRAVITY = 75;
            }
            else{
                this.MOUSE_BULLET_GRAVITY = 100;
            }
            if(this.autocannonCAP){
                this.MOUSE_CAPACITY = 4;
            }
            else{
                this.MOUSE_CAPACITY = 3;
            }
            if(this.autocannonROF){
                this.MOUSE_FIRE_DELAY = 75;
            }
            else{
                this.MOUSE_FIRE_DELAY = 100;
            }
            this.weaponName = "Autocannon";
            
            this.autocannonSprite.visible = true;
            this.vulcanSprite.visible = false;
            this.artillerySprite.visible = false;
            for(var i = 0; i < 30; i++){
                this.vulcanAmmoArray[i].visible = false;
            }
            for(var i = 0; i < this.MOUSE_CAPACITY; i++){
                this.autocannonAmmoArray[i].visible = true;
            }
            this.artilleryAmmo.visible = false;
        }
        else if(name == 'vulcan'){
            this.MOUSE_PROJECTILE = 'shot';
            this.MOUSE_BULLET_GRAVITY = 800;
            this.MOUSE_FIRE_DELAY = 15;
            this.MOUSE_SPREAD = 100;
            this.MOUSE_RECOIL = 5;
            if(this.vulcanVEL){
                this.MOUSE_BULLET_VELOCITY = 750;
            }
            else{
                this.MOUSE_BULLET_VELOCITY = 600;
            }
            if(this.vulcanCAP){
                this.MOUSE_CAPACITY = 30;
            }
            else{
                this.MOUSE_CAPACITY = 20;
            }
            
            if(this.vulcanREL){
                this.MOUSE_RELOAD_TIME = 750;
            }
            else{
                this.MOUSE_RELOAD_TIME = 1000;
            }
            this.weaponName = "Vulcan Cannon";
            this.autocannonSprite.visible = false;
            this.vulcanSprite.visible = true;
            this.artillerySprite.visible = false;
            for(var i = 0; i < 4; i++){
                this.autocannonAmmoArray[i].visible = false;
            }
            for(var i = 0; i < this.MOUSE_CAPACITY; i++){
                this.vulcanAmmoArray[i].visible = true;
            }
            this.artilleryAmmo.visible = false;
        }
        else if(name == 'artillery'){
            this.MOUSE_PROJECTILE = 'shell';
            this.MOUSE_CAPACITY = 1;
            this.MOUSE_FIRE_DELAY = 0;
            this.MOUSE_SPREAD = 0;
            this.MOUSE_RECOIL = 0;
            if(this.artilleryGRAV){
                this.MOUSE_BULLET_GRAVITY = 25;
            }
            else{
                this.MOUSE_BULLET_GRAVITY = 50;
            }
            if(this.artilleryREL){
                this.MOUSE_RELOAD_TIME = 1500;
            }
            else{
                this.MOUSE_RELOAD_TIME = 2000;
            }
            if(this.artilleryVEL){
                this.MOUSE_BULLET_VELOCITY = 600;
            }
            else{
                this.MOUSE_BULLET_VELOCITY = 400;
            }
            this.weaponName = "Artillery";
            this.autocannonSprite.visible = false;
            this.vulcanSprite.visible = false;
            this.artillerySprite.visible = true;
            for(var i = 0; i < 4; i++){
                this.autocannonAmmoArray[i].visible = false;
            }
            for(var i = 0; i < 30; i++){
                this.vulcanAmmoArray[i].visible = false;
            }
            this.artilleryAmmo.visible =true;
        }
        else{
            this.MOUSE_BULLET_GRAVITY = 50;
            this.MOUSE_BULLET_VELOCITY = 200;
            this.MOUSE_CAPACITY = 1;
            this.MOUSE_FIRE_DELAY = 400;
            this.MOUSE_SPREAD = 50;
            this.MOUSE_RECOIL = 10;
            this.MOUSE_RELOAD_TIME = 250;
            this.weaponName = "Unknown";
        }
        
        
    },
    
    loadQuestion: function(){
        
        var nQuestions = 10;
        var number = Math.floor(Math.random() *nQuestions);
        
        if(number == 0){
            this.question = "Fractions below 0.5";
            this.correctAnswers = ["1/3","2/5","3/9","4/7","45/100","2/6"];
            this.incorrectAnswers = ["3/5","6/7","3/4","2/3","45/80","4/6"];
        }
        else if(number == 1){
            this.question = "Percentages below 4/7";
            this.correctAnswers = ["22%","4%","18%","55%","40%","53%","39%"];
            this.incorrectAnswers = ["58%","64%","79%","60%","99%","81%"];
        }
        else if(number == 2){
            this.question = "Fractions above 60%";
            this.correctAnswers = ["77/100","3/4","5/6","3/2","6/9"];
            this.incorrectAnswers =["45/100","2/4","3/6","1/2","4/9"];
        }
        else if(number == 3){
            this.question = "Binary numbers < 4";
            this.correctAnswers = ["000","001","010","011"];
            this.incorrectAnswers = ["100","101","110","111"];
        }
        else if(number == 4){
            this.question = "Percentages below 4/16";
            this.correctAnswers = ["6%","20%","12%","4%","16%"];
            this.incorrectAnswers = ["28%","40%","66%","46%"];
        }
        else if(number == 5 || number==6){
            this.question = "Fractions above 90%";
            this.correctAnswers = ["93/100","9/9","3/2","48/50"];
            this.incorrectAnswers = ["7/9","33/66","90/101","1/2"];
        }
        else if(number == 7){
            this.question = "Whole numbers";
            this.correctAnswers = ["80/80","33/33","15/15","100/100"];
            this.incorrectAnswers = ["25/26","73/40","101/100","42/60"];
        }
        else if(number == 8){
            this.question = "Fractions equal to 50%";
            this.correctAnswers = ["4/8","2/4","16/32","32/64","1/2"];
            this.incorrectAnswers = ["3/4","4/6","2/3","16/64","32/16"];
        }
        else if(number == 9){
            this.question = "Percentages above 1/3";
            this.correctAnswers = ["35%","50%","66%","40%","100%"];
            this.incorrectAnswers = ["30%","25%","10%","3%"];
        }
       
        this.questionDisplay.text = this.question;
        
        
    }
}




