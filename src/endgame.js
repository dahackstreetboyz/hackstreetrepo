var endGame = function(game){
	endGame.game = game;

    this.NEXT_STATE = "MainMenu";

}

//file for displaying user scores
endGame.prototype = {
    
    preload: function(){
        this.game.load.image('endgame_title','assets/endgame/you_won_click.png');

    },
    
    create: function(){

        var backdrop = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY-150, 'alien_splash');
        backdrop.anchor.setTo(0.5,0.5);

        //the click mode to begin sprite
        this.you_won_sprite = this.game.add.sprite(50, 675, 'endgame_title');
        this.you_won_sprite.scale.setTo(.5,.5); 
        this.you_won_sprite.alpha = 0; //make it go bye-bye
        //tween to flash it in and out
        this.game.add.tween(this.you_won_sprite).to( { alpha: 1 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);

        //now get info from score
        var titleStyle = {font: "40px Arial", fill: "#7CFC00", align: "center" };
        var subTitleStyle = {font: "20px Arial", fill: "#7CFC00", align: "center" };

        
        // Mathsteroids Game
        this.mathsteroids_text = this.game.add.text((this.game.world.centerX)-200, (this.game.world.centerY-180),
            "Mathsteroids: " + this.game.mathsteroidsScore, titleStyle);
        this.mathsteroids_subHits = this.game.add.text((this.game.world.centerX)-150, (this.game.world.centerY-130),
            "UFOs Hit: ", subTitleStyle); // Add Data
        this.mathsteroids_subEnemiesLanded = this.game.add.text((this.game.world.centerX)-150, (this.game.world.centerY-100),
            "Enemies Landed: ", subTitleStyle); // Add Data
            

        // Basketball Game
        this.basketball_text = this.game.add.text((this.game.world.centerX)-200, (this.game.world.centerY-70),
            "Basketball Game: " + this.game.basketballScore, titleStyle);
        this.dance_subTaken = this.game.add.text((this.game.world.centerX)-150, (this.game.world.centerY-20),
            "Shots Taken: " + this.game.ballshotsTaken, subTitleStyle);
        this.dance_subMissed = this.game.add.text((this.game.world.centerX)-150, (this.game.world.centerY+10),
            "Shots Missed: " + this.game.ballshotsMissed, subTitleStyle);

        // Conveyor Score
        this.conveyor_text = this.game.add.text((this.game.world.centerX)-200, (this.game.world.centerY+50),
            "Conveyor Game: " + this.game.conveyorScore, titleStyle);
        this.conveyor_subQuestions = this.game.add.text((this.game.world.centerX)-150, (this.game.world.centerY+100),
            "Total Questions: " + this.game.numberOfConveyorQuestions, subTitleStyle);
        this.conveyor_subMissed = this.game.add.text((this.game.world.centerX)-150, (this.game.world.centerY+130),
            "Missed: " + this.game.numberOfConveyorMissed, subTitleStyle);

        // Dance Score
        this.dance_text = this.game.add.text((this.game.world.centerX)-200, (this.game.world.centerY+160),
            "Dancing Game: " + this.game.danceScore, titleStyle);
        


        
    },
    
    update: function() {

        if(this.game.input.activePointer.isDown){
                this.game.freeUnlocked = true;
                this.game.music.stop();
                this.game.state.start(this.NEXT_STATE);
            }
        
    }
    
    
}
