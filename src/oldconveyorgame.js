var conveyorGame = function(game){
    conveyorGame.game = game;
}

//groups
var tires;
var ufos;
var boxes;

//Question Vars
var randomNums = [];
var randomAnswers = [];
var actualAnswer;
var displayedAnswers = [];
var currentDisplayIndex = 0;
var questionText;
var questionString;

//statics
var TIRE_VELOCITY = -75;
var DIFFICULTY_LEVEL;

//other
var timer;
var score;
var scoreText;

conveyorGame.prototype =  {
    
    
    preload: function(){
        
    this.game.load.image('tiretexture', 'assets/conveyorgame/tiretexture.jpg');
    this.game.load.image('rectangle', 'assets/conveyorgame/rectangle.jpg');
    this.game.load.image('y_rectangle', 'assets/conveyorgame/yellow_rectangle.jpg');
    this.game.load.image('ufo', 'assets/conveyorgame/ufo.png');
    this.game.load.image('box', 'assets/conveyorgame/box.png');
    
    },
    
    create: function(){
        //currently hard-coded diff level
        DIFFICULTY_LEVEL = 1;
        
        //option for setting background color
        this.game.stage.backgroundColor = '#000000';
        
        //setup the score
        score = 0;
        var style = { font: "65px Arial", fill: "#ffffff", align: "center" };
        scoreText = this.game.add.text(50, 50, score, style);
        
        //get physics going
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        
        //add tire/conveyer texture group
        tires = this.game.add.group();
        tires.enableBody = true;
        
        //add ufos group
        ufos = this.game.add.group();
    
        boxes = this.game.add.group();
        boxes.enableBody = true;
    
        //add the shoot thing
        var y_rect = this.game.add.sprite(this.game.width - 225, this.game.height-64, 'y_rectangle');
    
        questionText = this.game.add.text(y_rect.x+50, y_rect.y+20, " ", { fontSize: '40px', fill: '#000' });
    
        //make a new row of conveyor belt
        this.addRowOfTires();
    
        //allow for timer
        timer = this.game.time.create(true);
        // timer = this.game.time.events.loop(5000, addRowOfTires, this); //currently there's no texture so we don't need this
        //plus i need to find a better way

        //make a new problem (this method calls methods with bugs)               !!!TODO: fix that!!!
        this.newProblem();
        
    },
    
    update: function() {
        //nothing for now, will probably need some shtuff later so leaving this here
    },
    
    addOneTire: function(x, y) {
        // Get the first dead pipe of our group
        var tire = tires.create(x,y,'rectangle');
    
        // Set the new position of the texture
        tire.reset(x, y);
        // Add velocity to the texture to make it go up
        // tire.body.velocity.y = this.TIRE_VELOCITY; 
    
        // Kill the tire texture when it's no longer visible 
        tire.checkWorldBounds = true;
        tire.outOfBoundsKill = true;
    },
    
    addRowOfTires: function() {
        // Add the 6 tires 
    for (var i = 0; i < 5; i++)
        this.addOneTire(this.game.width-200, i * 250);
    },
    
    newProblem: function() {
        // questionText.text = null;
        timer = null;                                                       //!!TODO FIX TIMER RESTART
        //remove all our boxes                                              //!!TODO FIND BETTER WAY
        if (boxes.length > 1) {
            for (var i=0; i < boxes.length; i ++) {
                boxes.getAt(i).kill();
            }
        }
        
        //nullify all our variables
        randomNums.length = 0;
        randomAnswers.length = 0;
        displayedAnswers.length = 0;
        currentDisplayIndex = 0;
        actualAnswer = null;
        //generate random problem and answers, and get real answer
        this.addProblem();
    
        //loop to generate boxes
        timer = this.game.time.events.loop(4000, this.generateBox, this);
    },
    
    addProblem: function() {
        var ufo = ufos.create(100, 200, 'ufo'); //make a new ufo
        ufo.scale.setTo(0.7,0.7); //make the ufo smaller
    
        switch(DIFFICULTY_LEVEL) { //choose a range based on difficulty
            case(3):
                var HIGH = 100;
                var LOW = 50;
                break;
            case(2):
                var HIGH = 50;
                var LOW = 25;
                break;
            case(1):
                var HIGH = 20;
                var LOW = 10;
    
        }
    
        //create our question numbers
        for (var i = 0; i < 3; i ++) {
        //create a random number from -LOW to +LOW
        var randomNumber = Math.floor(Math.random() * HIGH) - LOW;
        randomNums[i] = randomNumber;
        }
    
        //our question in string format
        questionString = randomNums[0] + " " + "-" + " " + randomNums[1] + " " + "-" + " " + randomNums[2];
        questionText.text = questionString; //makes the display text our question
        console.log(questionText.text); //debugging log
        console.log("Question: " + questionString); //debugging log
        // questionText.text = qString;
        //our answer
        actualAnswer = eval(randomNums[0] - randomNums[1] - randomNums[2]);
        console.log("answer: " + actualAnswer);
        randomAnswers[0] = actualAnswer;
        //now generate fake answers
        for (var i = 1; i < 10; i ++) {
            randomAnswers[i] = Math.floor(Math.random() * HIGH) - LOW;
        }
        for (var i = 0; i < randomAnswers.length; i++) {
            console.log(randomAnswers[i]);
        }
    },
    
    //this method has a bug when checking for an answer to display, when array
    //of displayed answers is full it infinitely loops and breaks
    generateBox: function() {
        //find a new answer
        console.log("genBox");
        var potentialAnswer = randomAnswers[Math.floor(Math.random() * 9)];
        if (displayedAnswers.indexOf(potentialAnswer) < 0) { //check if we've already shown it
            displayedAnswers[currentDisplayIndex] = potentialAnswer; //put this answer in the already shown array
            var box = boxes.create(this.game.width-150, this.game.height-100, 'box'); //make a sprite
            box.scale.setTo(.2,.2); //make it smaller
            box.body.velocity.y = TIRE_VELOCITY; //make it move
            box.value = potentialAnswer; //give it a value to hold onto 
            box.inputEnabled = true; //let us click it
            box.events.onInputDown.add(this.listener, this); //tell it what to do when clicked
            var display = this.game.add.text(150,175,box.value, {font: '200px Arial', fill: '#000' }); //make text out of the value
            box.addChild(display); //attach the text
            box.checkWorldBounds = true; //check bounds
            box.outOfBoundsKill = true; //kill when OOB
            currentDisplayIndex++; //increment displayed answers index
        } else { //this loop isn't executing **FIX**
            if (displayedAnswers.length > 9) { //we've displayed all possible answers
                displayedAnswers.length = 0;
                console.log("Displayed cleared")
                this.generateBox();
            }
            else {
                this.generateBox();
            }
        }
    },
    
    //listens for a click on an answer and acts
    listener: function(sprite, pointer) {
        if (sprite.value==actualAnswer) {
        score++;
        scoreText.text = score;
        this.newProblem();

    } else {
        score--;
        scoreText.text = score;
        sprite.kill();
        console.log("Incorrect.");
    }
    }
    
}