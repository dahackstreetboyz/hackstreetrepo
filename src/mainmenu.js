var mainmenu= function(game){}

var freePlayLock;

mainmenu.prototype = {

    preload: function() {
        //new splash images
        this.game.load.image('lock','assets/splashScreen/lock_gold.png');
        this.game.load.image('alien_splash','assets/splashScreen/alien_splash.jpg');
        this.game.load.image('new_game_img','assets/splashScreen/new_game_button.png');        
        this.game.load.image('free_play_img','assets/splashScreen/free_play_button.png');
        this.game.load.image('instruction_img', 'assets/splashScreen/splash_click_instr.png');
        this.game.load.image('schoolBuilding', 'assets/splashScreen/school.png');
        this.game.load.image('ufo', 'assets/splashScreen/ufo.png');

    },
    create: function(){

            var backdrop = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY-150, 'alien_splash');
            backdrop.anchor.setTo(0.5,0.5);
         
            this.makeButtons();
            
            this.freePlayLock = this.game.add.sprite(this.freeplay_button.x+140, this.freeplay_button.y+40, 'lock');
            this.freePlayLock.scale.setTo(.3,.3);

            this.ufo = this.game.add.sprite(-200, this.game.world.centerY-200, 'ufo');
            this.ufo.scale.setTo(.3,.3);
            this.ufo_tween = this.game.add.tween(this.ufo).to({ x: 1000 }, 4000, Phaser.Easing.Linear.None, true);
            this.ufo_tween.repeat(50, 3000);

            this.schoolBuilding = this.game.add.sprite(this.game.world.centerX-160, this.game.world.centerY-150, 'schoolBuilding');
            this.schoolBuilding.scale.setTo(.3,.3);       
            
            this.game.music = this.game.add.audio('backgroundMusic');
            this.game.music.loop = true;
            this.game.music.play();
    },
    
    update: function(){
        if(this.game.freeUnlocked==true){
            this.freePlayLock.visible = false;
        }
    },
    
    //make newgame, loadgame, and freeplay buttons
    makeButtons: function() {
        this.newgame_button = this.game.add.button(10, 500, 'new_game_img', this.goToNextState, this, 2, 1, 0);
        this.newgame_button.scale.setTo(0.85, 0.85);

        this.freeplay_button = this.game.add.button(370, 500, 'free_play_img', this.goToFreePlay, this, 2, 1, 0);
        this.freeplay_button.scale.setTo(0.85, 0.85);

        //the click mode to begin sprite
        this.instructionSprite = this.game.add.sprite(17, 725, 'instruction_img');
        this.instructionSprite.scale.setTo(.5,.5); 
        this.instructionSprite.alpha = 0; //make it go bye-bye
        //tween to flash it in and out
        this.game.add.tween(this.instructionSprite).to( { alpha: 1 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);
        
    },
    
    
    //go to the next state when button clicked, need to specify exact states later  -- you cant define functions like that
    goToNextState: function() {
       // this.game.music.loop=false;
        this.game.state.start("Intro");
    },
    
    loadGame:function(){
        //TODO
    },
    
    goToFreePlay: function(){
        if(this.game.freeUnlocked==true){
            this.game.music.stop();
            this.game.state.start("FreePlayMenu");
        }
        else{
            //give feedback on the screen
        }
    },
    
    over: function() {
        console.log("OVER NEW GAME");
    }
}