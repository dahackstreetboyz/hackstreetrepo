var characterCreation = function(game){}


var player;
//There is probably a better way to dynamically set these. For right now, they're just hard coded.
//When we update the spritesheets make sure to change these.
var N_HEADS = 22;
var N_PANTS = 6;
var N_SHIRTS = 10;
var headSprite;
var bodySprite;
var legSprite;
var headLeft;
var headRight;
var bodyLeft;
var bodyRight;
var legLeft;
var legRight;
var SELECTOR_LOC_X = 64;
var SELECTOR_LOC_Y = 64;
var headChoice =0;
var bodyChoice=0;
var legChoice=0;
var cursors;

characterCreation.prototype = {
    
    preload: function(){
        //Load the spritesheets like this, you will access them using the first argument's string literal.
        this.game.load.spritesheet('heads','assets/charCreation/heads_noOutline_32_32.png',32,32);
        this.game.load.spritesheet('shirts','assets/charCreation/shirts_32_32.png',32,32);
        this.game.load.spritesheet('pants','assets/charCreation/pants_32_32.png',32,32);
        this.game.load.image('left','assets/charCreation/leftArrow.png');
        this.game.load.image('right','assets/charCreation/rightArrow.png');
    },
    
    create: function(){
        
            cursors = this.game.input.keyboard.createCursorKeys();
            var introString = "Character creator -- Hit right arrow on keyboard to go to next state";
            var introDisplay = this.game.add.text(50,200,introString,{font: '12px Arial', fill: '#fff'});
            introDisplay.alpha =0.5;
            //The texture var is a special type of texture which allows us to draw directly to it on the fly.
            this.game.playerTexture = this.game.add.renderTexture(32,96,'PlayerTexture');
             //the actual sprites that we will draw to this texture need to be defined and added as sprites.
             //Note that I'm drawing them at 99999,99999 far off screen. We can't set them to invisible or else
             //we can't draw them to the texture.
            headSprite = this.game.add.sprite(99999,99999, 'heads');
            bodySprite = this.game.add.sprite(99999,99999, 'shirts');
            legSprite = this.game.add.sprite(99999,99999, 'pants');
            
            headLeft = this.game.add.sprite(SELECTOR_LOC_X-6, SELECTOR_LOC_Y, 'left');
            headLeft.inputEnabled = true;
            headLeft.input.useHandCursor = true;
            headLeft.events.onInputDown.add(this.headLeftListener,this);
            
            headRight = this.game.add.sprite(SELECTOR_LOC_X+70, SELECTOR_LOC_Y, 'right');
            headRight.inputEnabled = true;
            headRight.input.useHandCursor = true;
            headRight.events.onInputDown.add(this.headRightListener,this);
            
            bodyLeft = this.game.add.sprite(SELECTOR_LOC_X-6, SELECTOR_LOC_Y+32, 'left');
            bodyLeft.inputEnabled = true;
            bodyLeft.input.useHandCursor = true;
            bodyLeft.events.onInputDown.add(this.bodyLeftListener,this);
            
            bodyRight = this.game.add.sprite(SELECTOR_LOC_X+70, SELECTOR_LOC_Y+32, 'right');
            bodyRight.inputEnabled = true;
            bodyRight.input.useHandCursor = true;
            bodyRight.events.onInputDown.add(this.bodyRightListener,this);
            
            legLeft = this.game.add.sprite(SELECTOR_LOC_X-6, SELECTOR_LOC_Y+64, 'left');
            legLeft.inputEnabled = true;
            legLeft.input.useHandCursor = true;
            legLeft.events.onInputDown.add(this.legLeftListener,this);
            
            legRight = this.game.add.sprite(SELECTOR_LOC_X+70, SELECTOR_LOC_Y+64, 'right');
            legRight.inputEnabled = true;
            legRight.input.useHandCursor = true;
            legRight.events.onInputDown.add(this.legRightListener,this);
            
            
           
            this.setCharacter(2,4,6);
           
           
            //This is the actual player sprite that will be used for game logic. it's at 64,64 on screen
            //and has the renderTexture as its image.
            player = this.game.add.sprite(SELECTOR_LOC_X + 32, SELECTOR_LOC_Y, this.game.playerTexture);
            player.enableBody();
        
            
    },
    
    update: function(){
        
         if(cursors.left.isDown || cursors.right.isDown){
             
             
	    	
	    	this.game.state.start("Intro");
	    	
	    }
    },
    
    //This function selects which frame from each spritesheet will be drawn for each sprite(head, body, legs)
    //If you want multiple characters in a scene, you will need a separate head, body and leg sprite for each one
    //in additon to the sprite actually being drawn for the entire character.
    setCharacter: function(headc,bodyc,legc){
         //update book keeping if this is called manually
         headChoice = headc;
         bodyChoice = bodyc;
         legChoice = legc;
         
         //change the separate sprites to the right choices
         headSprite.frame = headc;
         bodySprite.frame = bodyc;
         legSprite.frame = legc;
         
         //Here is where the three sprites are "merged". The process of merging is just drawing them 
	     //to a renderTexture which is being used for the player sprite. 
	     //You only need to do this when changing the sprite.
	     this.game.playerTexture.renderXY(headSprite,0,0,true);
	     this.game.playerTexture.renderXY(bodySprite,0,32,false);
	     this.game.playerTexture.renderXY(legSprite,0,64,false);
    },
    
    //This function saves your head,body, and leg choices to the game so it can be saved/loaded later.
    saveCharacterToGame: function(){
        this.game.headChoice = headChoice;
        this.game.bodyChoice = bodyChoice;
        this.game.legChoice = legChoice;
    },
    
    headLeftListener: function(sprite, pointer){
        headChoice --;
        if(headChoice == -1){
            headChoice = N_HEADS-1;
        }
        this.setCharacter(headChoice,bodyChoice,legChoice);
    },
    bodyLeftListener: function(sprite, pointer){
        bodyChoice --;
        if(bodyChoice == -1){
            bodyChoice = N_SHIRTS-1;
        }
        this.setCharacter(headChoice,bodyChoice,legChoice);
    },
    legLeftListener: function(sprite, pointer){
        legChoice --;
        if(legChoice == -1){
            legChoice = N_PANTS-1;
        }
        this.setCharacter(headChoice,bodyChoice,legChoice);
    },
   headRightListener: function(sprite, pointer){
        headChoice ++;
        if(headChoice == N_HEADS){
            headChoice = 0;
        }
        this.setCharacter(headChoice,bodyChoice,legChoice);
    },
    bodyRightListener: function(sprite, pointer){
        bodyChoice ++;
        if(bodyChoice == N_SHIRTS){
            bodyChoice = 0;
        }
        this.setCharacter(headChoice,bodyChoice,legChoice);
    },
    legRightListener: function(sprite, pointer){
        legChoice ++;
        if(legChoice == N_PANTS){
            legChoice = 0;
        }
        this.setCharacter(headChoice,bodyChoice,legChoice);
    }
    
    
   
    
}