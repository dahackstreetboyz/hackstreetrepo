var dance = function(game){
}

//Input variables
var aKey;
var sKey;
var dKey;

//Sprites
var aLane;
var sLane;
var dLane;
var targetBox;
var end;
var stages;

//Groups
var targets;
var obstacle;
var obstacleGroup;

//Players
var player;
var alien;

//Spacing variables
var LANE_TOP = 40;
var LANE_LEFT = 25;
var LANE_GAP = 95; //includes the width of a lane (100px) this is the distance from the left of one lane to the left of the next lane.
var TARGET_BOX_Y = 620;
var TARGET_BOX_X = 20;
var TARGET_OFFSET_X = 10;
var TARGET_OFFSET_Y = 10;
var CORRECT_PERCENT = 50;
var lastTargetTimestamp ;
var TARGET_SPAWN_DELAY = 5000;
var TARGET_BOX_SIZE = 100;

var health = 100;

dance.prototype = {
    
    preload: function(){
        this.game.load.image('background', 'assets/dance/background.png');
        this.game.load.image('targetBox', 'assets/dance/targetBox.png'); 
        this.game.load.image('aLane','assets/dance/aLane.png');
        this.game.load.image('sLane','assets/dance/sLane.png');
        this.game.load.image('dLane','assets/dance/dLane.png');
        this.game.load.image('target', 'assets/dance/target.png');  
        this.game.load.image('end','assets/dance/end.png');
        this.game.load.spritesheet('cheers','assets/dance/cheers_128_128.png',128,128);
    },
    
    create: function(){
        
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        
        //Creates input keys
        aKey = this.game.input.keyboard.addKey(Phaser.Keyboard.A);
        sKey = this.game.input.keyboard.addKey(Phaser.Keyboard.S);
        dKey = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
        
        //Creates lanes
        aLane = this.game.add.sprite(LANE_LEFT + LANE_GAP*0,LANE_TOP,'aLane');
        sLane = this.game.add.sprite(LANE_LEFT + LANE_GAP*1,LANE_TOP,'sLane');
        dLane = this.game.add.sprite(LANE_LEFT + LANE_GAP*2,LANE_TOP,'dLane');
        targetBox = this.game.add.sprite(TARGET_BOX_X, TARGET_BOX_Y, 'targetBox');
        end = this.game.add.sprite(0,this.game.world.height - 50,'end')
        
        //Creates ground
        obstacleGroup = this.game.add.group();
        obstacleGroup.enableBody = true;
        var ground = obstacleGroup.create(0, this.game.world.height - 50, 'end');
        ground.scale.setTo(2, 2);
        ground.body.immovable = true;
        
        //Creates stages
        stages = this.game.add.group();
        stages.enableBody = true;
        
        //Creates Player
        //CREATE PLAYER
        player = this.game.add.sprite(560,400, 'dude');
        this.game.physics.arcade.enable(player);
        player.body.bounce.y = 0.2;
        player.body.gravity.y = 300;
        player.body.collideWorldBounds = true;
        player.animations.add('left', [0, 1, 2, 3], 10, true);
        player.animations.add('right', [5, 6, 7, 8], 10, true);
        
        player.animations.add('dance',[0,1,2,3,4,5,6,7,8,7,6,5,4,3,2,1],10,true);
        player.frame = 4;
        
        
        //CREATE ALIEN
        alien = this.game.add.sprite(560,200, 'dude');
        this.game.physics.arcade.enable(alien);
        alien.body.bounce.y = 0.2;
        alien.body.gravity.y = 300;
        alien.body.collideWorldBounds = true;
        alien.animations.add('left', [0, 1, 2, 3], 10, true);
        alien.animations.add('right', [5, 6, 7, 8], 10, true);
        alien.animations.add('dance',[0,1,2,3,4,5,6,7,8,7,6,5,4,3,2,1],10,true);
        alien.animations.play('dance');
    },
    
    update: function(){
        
    }
}