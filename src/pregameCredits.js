var pregameCredits = function(game){
    this.currentImage = 0;
    this.mouseUp = true;
    this.sprites = {};
    this.NEXT_STATE = "Intro";
    this.nSlides = 3;
    
}
//group of sprites
var sprites;
//the current sprite we are on
var currentSprite;

//for our text stuff
var text;
var index = 0;
var line = '';



var creditContent = [
    " ",
    "a game from da hackstreet boyz",
    " ",
    "that teaches fractions",
    "and other math stuffs",
    " ",
    "with help from artists",
    "danielle and kalina",
    "    "
];


pregameCredits.prototype = {
    
    
    preload:function(){
        this.game.load.image('cut-space', 'assets/cutscenes/space.jpg'); //a boring space background
    },
    
    create: function(){

        //get physics going
    	this.game.physics.startSystem(Phaser.Physics.ARCADE);
    	
    	//allowing for panning of our sprites
    	sprites = this.game.add.group();
    	sprites.enableBody = true;
    	
    	//add the string of the image to this array to go through
        this.sprites[0] = 'cut-space';

        //set our initial image
        this.setImage(this.currentImage);
        
        //make our text
        text = this.game.add.text(32, 380, '', { font: "30pt Courier", fill: "#19cb65", stroke: "#119f4e", strokeThickness: 2 });
        
        this.nextLine();
        
    },
    
    update: function(){
        this.checkForSpriteChange();
        
    },
    
    setImage: function(num){
        currentSprite = sprites.create(0, 0, this.sprites[this.currentImage]);
    	currentSprite.body.velocity.x = -10;
        
    },
    
    //checks for our sprite-change condition and changes sprite if true
    //if all sprites are done, we go to the next state
    checkForSpriteChange: function() {
        if(this.game.input.activePointer.isDown && this.mouseUp ==true){
            this.currentImage++;
            if(this.currentImage >= this.nSlides){
                this.game.state.start(this.NEXT_STATE);
            }
            else{
                this.setImage(this.currentImage);
            }
            this.mouseUp = false;
        }
        else if(this.game.input.activePointer.isUp){
            this.mouseUp = true;
        }
    },
    
    nextLine: function() {
    index++;
    if (index < creditContent.length)
        {
            line = '';
            this.game.time.events.repeat(80, creditContent[index].length + 1, this.updateLine, this);
        }
    },
    
    updateLine: function() {

    if (line.length < creditContent[index].length)
    {
                console.log("hit this class");
        line = creditContent[index].substr(0, line.length + 1);
        // text.text = line;
        text.setText(line);
    }
    else
    {
        //  Wait 2 seconds then start a new line
        this.game.time.events.add(Phaser.Timer.SECOND * 2, this.nextLine, this);
    }

}
}