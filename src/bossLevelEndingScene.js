var bossLevelEndingScene = function(game){
    this.currentImage = 0;
    this.mouseUp = true;
    this.sprites = {};
    this.NEXT_STATE = "MainMenu";
    this.nSlides = 1;
}



bossLevelEndingScene.prototype = {
    
    preload:function(){
        this.game.load.image('cut9','assets/cutscenes/ending.jpg');
    },
    
    create: function(){
        this.sprites[0] = this.game.add.sprite(0,0,'cut9');
        this.sprites[0].visible = false;
        this.setImage(0);
    },
    
    update: function(){
        if(this.game.input.activePointer.isDown && this.mouseUp ==true){
            this.currentImage++;
            if(this.currentImage >= this.nSlides){
                this.game.freeUnlocked = true;
                this.game.music.stop();
                this.game.state.start(this.NEXT_STATE);
            }
            else{
                this.setImage(this.currentImage);
            }
            this.mouseUp = false;
        }
        else if(this.game.input.activePointer.isUp){
            this.mouseUp = true;
        }
    },
    
    setImage: function(num){
        this.sprites[num].visible = true;
    }
}