var dance = function(game){
}

//Input variables
var aKey;
var sKey;
var dKey;

//Sprites
var aLane;
var sLane;
var dLane;
var targetBox;
var end;
var stages;

//Displays
var qDisplay;
var healthDisplay;
var scoreDisplay;

var qStr = "What is: \n";
var qSlash = "/";
var qDenom = 10;
var qQuestion = "?";
var health = 100;
var healthStr = "Health: ";
var score = 0;
var scoreStr = "Score: ";

//Groups
var targets;
var targetBoxGroup;
var obstacle;
var obstacleGroup;

//Players
var player;
var alien;

//Spacing variables
var LANE_TOP = 40;
var LANE_LEFT = 25;
var LANE_GAP = 95; //includes the width of a lane (100px) this is the distance from the left of one lane to the left of the next lane.
var TARGET_BOX_Y = 620;
var TARGET_BOX_X = 20;
var TARGET_OFFSET_X = 10;
var TARGET_OFFSET_Y = 10;
var CORRECT_PERCENT = 50;
var lastTargetTimestamp ;
var TARGET_SPAWN_DELAY = 4000;
var TARGET_BOX_SIZE = 100;
var TARGET_VELOCITY = 40;

//Question Variables
var numerator;
var denominator;
var wrong1;
var wrong2;
var ansList;
var correctAnsInd;
var qAns;
var lanePick = 0;

var health = 100;
var timeCheck;

var scoreIncr = 15;
var lastClick = 0;

var timer;
var timerEvent;
var text;
var cheerSprite;
var N_CHEERS = 3;
var cheerTimestamp;

dance.prototype = {
    
    preload: function(){
        this.game.load.image('background', 'assets/dance/background.png');
        this.game.load.image('targetBox', 'assets/dance/targetBox.png'); 
        this.game.load.image('aLane','assets/dance/aLane.png');
        this.game.load.image('sLane','assets/dance/sLane.png');
        this.game.load.image('dLane','assets/dance/dLane.png');
        this.game.load.image('target', 'assets/dance/target.png');  
        this.game.load.image('end','assets/dance/end.png');
        this.game.load.spritesheet('cheers','assets/dance/cheers_128_128.png',128,128);
    },
    
    create: function(){
        
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        
        //Creates input keys
        aKey = this.game.input.keyboard.addKey(Phaser.Keyboard.A);
        aKey.onDown.add(function() {this.ansCheck(0)},this);
        sKey = this.game.input.keyboard.addKey(Phaser.Keyboard.S);
        sKey.onDown.add(function() {this.ansCheck(1)},this);
        dKey = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
        dKey.onDown.add(function() {this.ansCheck(2)},this);
        
        //Creates lanes
        aLane = this.game.add.sprite(LANE_LEFT + LANE_GAP*0,LANE_TOP,'aLane');
        sLane = this.game.add.sprite(LANE_LEFT + LANE_GAP*1,LANE_TOP,'sLane');
        dLane = this.game.add.sprite(LANE_LEFT + LANE_GAP*2,LANE_TOP,'dLane');
        
        targetBoxGroup = this.game.add.group();
        targetBoxGroup.enableBody = true;
        targetBox = targetBoxGroup.create(TARGET_BOX_X,TARGET_BOX_Y,'targetBox')
        
        end = this.game.add.sprite(0,this.game.world.height - 50,'end')
        
        //Creates ground
        obstacleGroup = this.game.add.group();
        obstacleGroup.enableBody = true;
        var ground = obstacleGroup.create(0, this.game.world.height - 50, 'end');
        ground.scale.setTo(2, 2);
        ground.body.immovable = true;
        
        //Creates stages
        stages = this.game.add.group();
        stages.enableBody = true;
        
        var stage1 = stages.create(400,300,'end');
        stage1.scale.setTo(0.4,0.5);
        stage1.body.immovable = true;
    
        var stage2 = stages.create(400,600,'end');
        stage2.scale.setTo(0.4,0.5);
        stage2.body.immovable = true;
        
        //Creates targets
        targets = this.game.add.group();
        targets.enableBody = true;
        
        //Creates Player
        player = this.game.add.sprite(560,400, 'dude');
        this.game.physics.arcade.enable(player);
        player.body.bounce.y = 0.2;
        player.body.gravity.y = 300;
        player.body.collideWorldBounds = true;
        player.animations.add('left', [0, 1, 2, 3], 10, true);
        player.animations.add('right', [5, 6, 7, 8], 10, true);
        player.animations.add('dance',[0,1,2,3,4,5,6,7,8,7,6,5,4,3,2,1],10,true);
        player.frame = 4;
        
        
        //Creates alien
        alien = this.game.add.sprite(560,200, 'dude');
        this.game.physics.arcade.enable(alien);
        alien.body.bounce.y = 0.2;
        alien.body.gravity.y = 300;
        alien.body.collideWorldBounds = true;
        alien.animations.add('left', [0, 1, 2, 3], 10, true);
        alien.animations.add('right', [5, 6, 7, 8], 10, true);
        alien.animations.add('dance',[0,1,2,3,4,5,6,7,8,7,6,5,4,3,2,1],10,true);
        alien.animations.play('dance');
        
        //Create displays
        qDisplay = this.game.add.text(300,10,qStr,{font: '20px Arial', fill: '#fff'});
        healthDisplay = this.game.add.text(10,10,healthStr+health,{font: '15px Arial', fill: '#fff'});
        scoreDisplay = this.game.add.text(100,10,scoreStr+score,{font: '15px Arial', fill: '#fff'});
        cheerSprite = this.game.add.sprite(500,350,'cheers');
        cheerSprite.frame = 0;
        cheerSprite.visible = false;
        
        //Creates timer
        timer = this.game.time.create();
        timerEvent = timer.add(Phaser.Timer.MINUTE * 3 + Phaser.Timer.SECOND * 0, this.endTimer, this);
        timer.start();
        
        //Adds music
        this.game.music = this.game.add.audio('danceTheme');
        this.game.music.loop = true;
        this.game.music.play();
        
        lastTargetTimestamp = this.game.time.now;
        this.addTarget();
    },
    
    update: function(){
        this.updatePhysics();
        this.gameUpdateLoop();
        
        if(this.game.time.now > cheerTimestamp+1000){
            cheerSprite.visible = false;
        }
    },
    
    render: function () {
        // If our timer is running, show the time in a nicely formatted way, else show 'Done!'
        if (timer.running) {
            this.game.debug.text(this.formatTime(Math.round((timerEvent.delay - timer.ms) / 1000)), 750, 14, "#ff0");
        }
        else {
            this.endGame(true);
        }
        },
        endTimer: function() {
            // Stop the timer when the delayed event triggers
            timer.stop();
        },
        formatTime: function(s) {
            // Convert seconds (s) to a nicely formatted and padded time string
            var minutes = "0" + Math.floor(s / 60);
            var seconds = "0" + (s - minutes * 60);
            return minutes.substr(-2) + ":" + seconds.substr(-2);   
    },
    
    updatePhysics: function(){
        //this.game.physics.arcade.overlap(targets, targetBoxGroup, this.targetHitsBox,null,this);
        this.game.physics.arcade.overlap(targets, obstacleGroup, this.targetHitsObstacle, null, this);
        this.game.physics.arcade.collide(player, stages);
        this.game.physics.arcade.collide(alien, stages);
    },
    
    gameUpdateLoop: function(){
        this.updateDisplays();
        this.addTarget();
        this.scoreListener();
    },
    
    updateDisplays: function(){
        healthDisplay.text = healthStr+health;
        scoreDisplay.text = scoreStr+score;
    },
    
    //Target creation
    addTarget: function(){
        
        this.newQuestion();
        var ansInd = [0,1,2];
        ansInd = this.fischerYatesShuffle(ansInd);
        
        if(this.game.time.now > lastTargetTimestamp+TARGET_SPAWN_DELAY){
            lastTargetTimestamp = this.game.time.now;
            for(var i=0;i<3;i++){
                    var xLoc = LANE_LEFT + TARGET_OFFSET_X + (LANE_GAP* i);
                    var yLoc = LANE_TOP + TARGET_OFFSET_Y;
                    var newTarget = targets.create(xLoc, yLoc, 'target');
                    newTarget.body.velocity.y = TARGET_VELOCITY;
                    newTarget.lane = i;
                    var ind = ansInd[i];
                    var display = this.game.add.text(5,25,ansList[ind], {font: '18px Arial', fill: '#fff' });
                    newTarget.addChild(display);
                    if(ind == 0){
                        newTarget.isCorrect = true;
                    }
                    else{
                        newTarget.isCorrect = false;
                    }
            }
            
            var ansTarget = targets.create(300,LANE_TOP+TARGET_OFFSET_Y,'target');
            ansTarget.body.velocity.y = TARGET_VELOCITY;
            var ansDisplay = this.game.add.text(5,25,qAns,{font: '16px Arial', fill: '#fff' });
            ansTarget.addChild((ansDisplay));
        }
    },
    
    newQuestion: function(){
        var numeratorList = [0,1,2,3,4,5,6,7,8,9,10];
        var denominatorList = [1,2,4,5,8,10];
        var ansNum = numeratorList[Math.floor(Math.random()*(numeratorList.length-1))];
        var ansDenom = denominatorList[Math.floor(Math.random()*(denominatorList.length-1))];
        var wrongAnsList = [];
        var wrongAns;
        var wrongListInd = 0;
        var decAns;
        
        
        while(wrongAnsList.length < 2){
            wrongAns = (Math.random()).toFixed(3);
            if(wrongAns != (ansNum/ansDenom).toFixed(3)){
                wrongAnsList[wrongListInd] = wrongAns;
                wrongListInd++;
            }
        }
        decAns = (ansNum/ansDenom).toFixed(3);
        numerator = ansNum;
        denominator = ansDenom;
        wrong1 = wrongAnsList[0];
        wrong2 = wrongAnsList[1];
        
        switch(Math.floor(Math.random()*4)){
            case 1:
                //question=fraction, ans=decimal
                qAns = numerator+"/"+denominator;
                ansList = [decAns,wrong1,wrong2];
                break;
            case 2:
                //question=fraction, ans=percent
                qAns = numerator+"/"+denominator;
                ansList = [((decAns*100).toFixed(1)+"%"),((wrong1*100).toFixed(1)+"%"),((wrong2*100).toFixed(1)+"%")];
                break;
            case 3:
                //question=decimal, ans=percent
                qAns = decAns;
                ansList = [((decAns*100).toFixed(1)+"%"),((wrong1*100).toFixed(1)+"%"),((wrong2*100).toFixed(1)+"%")];
                break;
            case 4:
                //question=percent, ans=decimal
                qAns = (decAns*100)+"%";
                ansList = [(decAns),(wrong1),(wrong2)];
                break;
        }
        
        
    },
    
    ansCheck: function(userLane){
        lanePick = userLane;
        if(this.game.time.now > lastClick+500){
            lastClick = this.game.time.now;
            if(this.game.physics.arcade.overlap(targets, targetBoxGroup, this.targetHitsBox,null,this)){}
            else{
                this.healthDec(15);
            }
        }
    },
    
    //Collision functions
    targetHitsObstacle: function(target, obstacle){
        if(target.isCorrect == true){
            this.healthDec(30);
            target.isCorrect = false;
        }
        target.kill();
    },
    
    //Target hits target box
    targetHitsBox: function(target, targetBox){
        if(target.isCorrect==true){
            if(target.lane == lanePick){
                if(target.y > TARGET_BOX_Y && target.y < TARGET_BOX_Y+TARGET_BOX_SIZE){
                    target.isCorrect = false;
                    score += scoreIncr;
                    this.healthAdd(10);
                    cheerSprite.visible = true;
                    target.kill();
                    player.animations.play('dance');
                }
            }
            else{
                this.healthDec(15);
            }
        }
    },
    
    fischerYatesShuffle: function(array){
      	var currentIndex = array.length, temporaryValue, randomIndex ;

      	// While there remain elements to shuffle...
      	while (0 !== currentIndex) {
   	 
        	// Pick a remaining element...
        	randomIndex = Math.floor(Math.random() * currentIndex);
        	currentIndex -= 1;
   	 
        	// And swap it with the current element.
        	temporaryValue = array[currentIndex];
        	array[currentIndex] = array[randomIndex];
        	array[randomIndex] = temporaryValue;
      	}
   	 
      	return array;
	},
	
	healthAdd: function(num){
        health += num;
        if(health > 100){
            health = 100;
        }
    },
    
    healthDec: function(num){
        //DECREMENTS HEALTH
        player.animations.stop();
        timeCheck = -5000;
        player.frame = 4;
        health -= num;
        if(health <= 0){
            health = 0;
            this.endGame(false);
        }
    },
    
    scoreListener: function(){
        if(score >= 100){
            this.endGame(true);
        }
    },
    
    endGame: function(win){
        this.game.danceScore = score;
        if(win == true){
            this.game.state.start('DanceEnding');
        }
        if(win == false){
            this.game.state.start("MainMenu");
        }
    }
}
