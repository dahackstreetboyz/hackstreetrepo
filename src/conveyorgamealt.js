var conveyorGame = function(game){
    conveyorGame.game = game;
}

//groups
var tires;
var ufos;
var boxes;
var topLine;

//Question Vars
var randomNums = [];
var randomAnswers = [];
var actualAnswer;
var displayedAnswers = [];
var currentDisplayIndex = 0;
var questionText;
var questionString;

//statics
var TIRE_VELOCITY = -250;
var DIFFICULTY_LEVEL;  
var BOX_GAP = 200;
        

//other
var timer;
var score;
var scoreText;

conveyorGame.prototype =  {
    
    
    preload: function(){
        
        this.game.load.image('tiretexture', 'assets/conveyorgame/tiretexture.jpg');
        this.game.load.image('rectangle', 'assets/conveyorgame/rectangle.jpg');
        this.game.load.image('y_rectangle', 'assets/conveyorgame/yellow_rectangle.jpg');
        this.game.load.image('ufo', 'assets/conveyorgame/ufo.png');
        this.game.load.image('box', 'assets/conveyorgame/box.png');
        this.game.load.image('topLine','assets/conveyorgame/topLine.png');
    
    },
    
    create: function(){
        //currently hard-coded diff level
        DIFFICULTY_LEVEL = 1;
        
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        
        //option for setting background color
        this.game.stage.backgroundColor = '#000000';
        
        //setup the score
        score = 0;
        var style = { font: "65px Arial", fill: "#ffffff", align: "center" };
        scoreText = this.game.add.text(50, 50, score, style);
        
        //get physics going
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        
        //add tire/conveyer texture group
        tires = this.game.add.group();
        tires.enableBody = true;
        
        //This is the line at the top of the conveyor used to move the boxes back to the bottom.
        topLine = this.game.add.group();
        topLine.enableBody = true;
        var newLine = topLine.create(625,-50,'topLine');
        newLine.body.immovable = true;
        
        //add ufos group
        ufos = this.game.add.group();
    
        boxes = this.game.add.group();
        boxes.enableBody = true;
    
        //add the shoot thing
        var y_rect = this.game.add.sprite(this.game.width - 225, this.game.height-64, 'y_rectangle');
    
        questionText = this.game.add.text(y_rect.x+50, y_rect.y+20, " ", { fontSize: '40px', fill: '#000' });
    
        //make a new row of conveyor belt
        this.addRowOfTires();
    
        //allow for timer
        timer = this.game.time.create(true);
        // timer = this.game.time.events.loop(5000, addRowOfTires, this); //currently there's no texture so we don't need this
        //plus i need to find a better way

        //make a new problem (this method calls methods with bugs)               !!!TODO: fix that!!!
        this.newProblem();
        
    },
    
    update: function() {
         
        	this.game.physics.arcade.overlap(boxes,topLine,this.resetBox,null,this);
    },
    
    addOneTire: function(x, y) {
        // Get the first dead pipe of our group
        var tire = tires.create(x,y,'rectangle');
    
        // Set the new position of the texture
        tire.reset(x, y);
        // Add velocity to the texture to make it go up
        // tire.body.velocity.y = this.TIRE_VELOCITY; 
    
        // Kill the tire texture when it's no longer visible 
        tire.checkWorldBounds = true;
        tire.outOfBoundsKill = true;
    },
    
    addRowOfTires: function() {
        // Add the 6 tires 
    for (var i = 0; i < 5; i++)
        this.addOneTire(this.game.width-200, i * 250);
    },
    
    newProblem: function() {
        // questionText.text = null;
        timer = null;                                                       //!!TODO FIX TIMER RESTART
        //remove all our boxes                                              //!!TODO FIND BETTER WAY
        if (boxes.length > 1) {
            for (var i=0; i < boxes.length; i ++) {
                boxes.getAt(i).kill();
            }
        }
        
        //nullify all our variables
        randomNums.length = 0;
        randomAnswers.length = 0;
        displayedAnswers.length = 0;
        currentDisplayIndex = 0;
        actualAnswer = null;
        //generate random problem and answers, and get real answer
        this.addProblem();
    
        //loop to generate boxes
       // timer = this.game.time.events.loop(4000, this.generateBox, this);
         this.generateBoxes();
    },
    
    addProblem: function() {
        var ufo = ufos.create(100, 200, 'ufo'); //make a new ufo
        ufo.scale.setTo(0.7,0.7); //make the ufo smaller
    
        switch(DIFFICULTY_LEVEL) { //choose a range based on difficulty
            case(3):
                var HIGH = 100;
                var LOW = 50;
                break;
            case(2):
                var HIGH = 50;
                var LOW = 25;
                break;
            case(1):
                var HIGH = 20;
                var LOW = 10;
    
        }
    
        //create our question numbers
        for (var i = 0; i < 3; i ++) {
        //create a random number from -LOW to +LOW
        var randomNumber = Math.floor(Math.random() * HIGH) - LOW;
        randomNums[i] = randomNumber;
        }
    
        //our question in string format
        questionString = randomNums[0] + " " + "-" + " " + randomNums[1] + " " + "-" + " " + randomNums[2];
        questionText.text = questionString; //makes the display text our question
        console.log(questionText.text); //debugging log
        console.log("Question: " + questionString); //debugging log
        // questionText.text = qString;
        //our answer
        actualAnswer = eval(randomNums[0] - randomNums[1] - randomNums[2]);
        console.log("answer: " + actualAnswer);
        randomAnswers[0] = actualAnswer;
        //now generate fake answers
        for (var i = 1; i < 10; i ++) {
            randomAnswers[i] = Math.floor(Math.random() * HIGH) - LOW;
        }
        for (var i = 0; i < randomAnswers.length; i++) {
            console.log(randomAnswers[i]);
        }
    },
    
 
    generateBoxes: function() {
        //find a new answer

      //  randomAnswers = this.fischerYatesShuffle(randomAnswers);    //shuffle the random answers
     
        for(var i = 0; i < randomAnswers.length; i++){  //iterate over the answers, for each spawn a new box
      
            //Create the box sprite and attach the string to it
            
                var box = boxes.create(this.game.width-150, this.game.height-100+ BOX_GAP*i, 'box'); //make a sprite  -- why so many magic numbers?
                box.scale.setTo(.2,.2); //make it smaller
                box.body.velocity.y = TIRE_VELOCITY; //make it move
                box.value = randomAnswers[i]; //give it a value to hold onto 
                box.inputEnabled = true; //let us click it
                box.events.onInputDown.add(this.listener, this); //tell it what to do when clicked
                
                
                var display = this.game.add.text(150,175,box.value, {font: '200px Arial', fill: '#000' }); //make text out of the value
                box.addChild(display); //attach the text
        }
           
        
    },
    
    //listens for a click on an answer and acts
    listener: function(sprite, pointer) {
        if (sprite.value==actualAnswer) {
        score++;
        scoreText.text = score;
        this.newProblem();

        } else {
            score--;
            scoreText.text = score;
            sprite.kill();
            console.log("Incorrect.");
        }
    },
    
    fischerYatesShuffle: function(array){
          var currentIndex = array.length, temporaryValue, randomIndex ;

          // While there remain elements to shuffle...
          while (0 !== currentIndex) {
        
            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
        
            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
          }
        
          return array;
    },
    
    resetBox: function(box, line){
        box.y = this.game.height + BOX_GAP;
    }
    
}