var preload= function(game){}

preload.prototype = {
    preload: function() {
        
        this.game.load.audio('backgroundMusic','assets/splashScreen/defcon_zero_by_paragonx9.ogg');
        
        
        //MATHSTEROIDS
        
        this.game.load.image('sky', 'assets/mathsteroids/background.png');            
        this.game.load.image('star', 'assets/mathsteroids/star.png');
        this.game.load.image('bullet','assets/mathsteroids/bullet.png');
        this.game.load.image('diamond','assets/mathsteroids/diamond.png');
        this.game.load.image('ground', 'assets/mathsteroids/platform.png');
        this.game.load.image('cannonball','assets/mathsteroids/cannonball.png');
        this.game.load.image('shot','assets/mathsteroids/shot.png');
        this.game.load.spritesheet('shell','assets/mathsteroids/firebullet.png',32,32);
        //this.game.load.image('shell','assets/artilleryShell.png');
        this.game.load.spritesheet('dude', 'assets/mathsteroids/greendude.png', 32, 48);
        //this.game.load.image('dude','assets/ship.png');
        this.game.load.spritesheet('achievementSprite','assets/mathsteroids/achievements_32_32.png',32,32);
      /*  this.game.load.image('t0', 'assets/mathsteroids/tutorial/tut0.png');
        this.game.load.image('t1', 'assets/mathsteroids/tutorial/tut1.png');
        this.game.load.image('t2', 'assets/mathsteroids/tutorial/tut2.png');
        this.game.load.image('t3', 'assets/mathsteroids/tutorial/tut3.png');
        this.game.load.image('t4', 'assets/mathsteroids/tutorial/tut4.png');
        this.game.load.image('t5', 'assets/mathsteroids/tutorial/tut5.png');
        this.game.load.image('t6', 'assets/mathsteroids/tutorial/tut6.png');
        this.game.load.image('t7', 'assets/mathsteroids/tutorial/tut7.png');
        this.game.load.image('t8', 'assets/mathsteroids/tutorial/tut8.png');
        this.game.load.image('t9', 'assets/mathsteroids/tutorial/tut9.png');
        this.game.load.image('t10', 'assets/mathsteroids/tutorial/tut10.png');
        this.game.load.image('t11', 'assets/mathsteroids/tutorial/tut11.png');
        this.game.load.image('t12', 'assets/mathsteroids/tutorial/tut12.png');
        this.game.load.image('t13', 'assets/mathsteroids/tutorial/tut13.png');
        this.game.load.image('t14', 'assets/mathsteroids/tutorial/tut14.png');
        this.game.load.image('t15', 'assets/mathsteroids/tutorial/tut15.png');
        this.game.load.image('t16', 'assets/mathsteroids/tutorial/tut16.png');
        this.game.load.image('t17', 'assets/mathsteroids/tutorial/tut17.png');
        this.game.load.image('t18', 'assets/mathsteroids/tutorial/tut18.png');
        
        */
        
        this.game.load.image('instructions','assets/mathsteroids/mathsteroidsShortTutorial.png');
        this.game.load.image('controls','assets/mathsteroids/mathsteroidscontrols.png');
        this.game.load.image('top','assets/mathsteroids/topBar.png');
        
        this.game.load.spritesheet('UFOsprite','assets/mathsteroids/ufo.gif',58,35 );
        this.game.load.spritesheet('alienSprite','assets/mathsteroids/alienSheet.png',90,128);
        this.game.load.image('vulcanSprite','assets/mathsteroids/minigun_32_32.png');
        this.game.load.image('autocannonSprite','assets/mathsteroids/autocannon_32_32.png');
        this.game.load.image('artillerySprite','assets/mathsteroids/artillery_32_32.png');
        this.game.load.image('timeLeftSprite','assets/mathsteroids/time_left_128_32.png');
        this.game.load.image('vulcanShell','assets/mathsteroids/shell_8_32.png');
        this.game.load.image('autocannonShell','assets/mathsteroids/shell_32_64.png');
        this.game.load.image('artilleryShell','assets/mathsteroids/shell_32_128.png');
        
        this.game.load.audio('mathsteroidsTheme','assets/mathsteroids/569830_Action-Hero-Loop.ogg');
        
        
        //BASKETBALL
        
        
        this.game.load.audio('basketballTheme','assets/basketball/563649_Warp-Field.ogg');
        
        //CONVEYOR
        
        this.game.load.audio('conveyorTheme','assets/conveyorgame/620721_Awakening-the-cloud.ogg');
        
        //DANCE
        
        this.game.load.audio('danceTheme','assets/dance/608701_8bits-Soft-Flow.ogg');
        
    },
    create: function() {
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
        this.scale.setScreenSize( true );

        this.game.state.start('MainMenu'); 
       //this.game.state.start("Conveyor");
    }
}