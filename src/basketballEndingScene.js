var basketballEndingScene = function(game){
    this.currentImage = 0;
    this.sprites = {};
    this.canClick = false;
    this.NEXT_STATE = "Conveyor";
    this.sprites; //group of sprites
    this.currentSprite; //the current sprite we are on
    this.text;
    this.index = 0; //the index of our text content
    this.line; //the current line being displayed as text
    //the text to show to introduce the game theme
    this.content = [
    " ",
    "nice shots!",
    "the aliens are humiliated",
    " ",
    "they say they want to leave...",
    "but their ships are broken",
    " ",
    "help them fix their ships",
    "by solving integer problems",
    " ",
    "click to help them out.."
    ];
}

basketballEndingScene.prototype = {
    
    preload:function(){
        this.game.load.image('need-help','assets/cutscenes/cut_scene_need_help.jpg');
    },
    
    create: function(){
         //get physics going
    	this.game.physics.startSystem(Phaser.Physics.ARCADE);
    	
    	//allowing for panning of our sprites
    	this.sprites = this.game.add.group();
    	this.sprites.enableBody = true;
    	
    	//add the string of the image to this array to go through
        this.sprites[0] = 'need-help';

        //set our initial image
        this.setImage(this.currentImage); //make our text
        this.text = this.game.add.text(32, 50, '', {
            font: "30pt Courier",
            fill: "#ff00000",
            stroke: "#119f4e",
            strokeThickness: 2
        });

        //get the next line from content
        this.nextLine();
        
    },
    
   update: function() {
        if (this.canClick == true && this.game.input.activePointer.isDown) {
            this.game.music.stop();
            this.game.state.start(this.NEXT_STATE);
        }
    },

    //sets the image of the given currentImage
    setImage: function(num) {
        this.currentSprite = this.sprites.create(0, 0, this.sprites[num]);
        this.currentSprite.scale.setTo(1.1, 1.1);
        this.currentSprite.body.velocity.x = -20;
    },

    //gets the next line from content, changes our image if the next line
    //describes the next image
    nextLine: function() {
        this.index++;
        if (this.index < this.content.length) //if we have more content, get next line
        {
            this.line = '';
            this.game.time.events.repeat(80, this.content[this.index].length + 1, this.updateLine, this);
        }
        if (this.index >= this.content.length) {
            this.canClick = true;
        }
        
    },

    //update the text with our new line
    updateLine: function() {
        if (this.line.length < this.content[this.index].length) {
            this.line = this.content[this.index].substr(0, this.line.length + 1);
            // text.text = line;
            this.text.setText(this.line);
        }
        else {
            //  Wait 1 second then start a new line
            this.game.time.events.add(Phaser.Timer.SECOND * 1, this.nextLine, this);
        }

    }
    
}