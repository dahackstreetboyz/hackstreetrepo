var basketball = function(game){}

var HEIGHT = 600;
var WIDTH = 800;
var basket;
var balls;
var fakeBall;
var player;
var wKey;
var aKey;
var sKey;
var dKey;
var spacebar;
var topLine;
var holdingBall = false;
var shootTimestamp;
var SHOOT_COOLDOWN = 1000;
var poweringUp = false;
var poweringDown = false;
var power;
var MAX_POWER = 65;
var basketballScore = 0;
var scoreString = "Score: ";
var scoreDisplay;
var powerString = "Power: ";
var powerDisplay;
var haveScored = false;
var hitBox;
var background;
var crowd1;
var crowd2;
var controlString = "WASD to move\nRun over ball to pick up\nHold space to charge, aim with mouse\n";
var controlDisplay;
var grid;
var GRID_BOX_SIZE = 64;
var GRID_TOP = 384;
var GRID_LEFT = 192;
var GRID_X_SIZE = 6;
var GRID_Y_SIZE = 6;
var GRID_RIGHT = (GRID_LEFT + (GRID_X_SIZE * GRID_BOX_SIZE));
var GRID_BOTTOM =  (GRID_TOP + (GRID_Y_SIZE * GRID_BOX_SIZE));
var LINE_SIZE = 4;
var playerGridXLocation = 0;
var playerGridYLocation = 0;
var questionXLocation;
var questionYLocation;
var questionXY;
var questionDisplay;
var questionString = " ";
var playerOnGrid = false;
var locationDisplay;
var locationString = " ";
var haveShot = true;
var wrongspotDisplay;
var wrongspotString = " ";
var realismMode = false;
var TARGET_RADIUS = 32;
var testShouldScore = " True";
var testShouldScoreDisplay;
var shouldScore;
var SCORETOWIN = 5;
var powerBar;
//var ballshotsTaken = 0;
//var ballshotsMissed = 0;

var tut0Sprite;
var tut1Sprite;
var tutorialStage = 0;
var lastClickTimestamp;

var horseSpriteArray = [];


basketball.prototype = {
    
    preload: function(){
        	this.game.load.spritesheet('dude','assets/basketball/dude.png',32,48);
	        this.game.load.image('basket','assets/basketball/hoop.png');
        	this.game.load.image('background','assets/basketball/canvas.png');
        	this.game.load.image('line','assets/basketball/line.png');
        	this.game.load.spritesheet('ball','assets/basketball/basketball.png',32,32);
        	this.game.load.image('target','assets/basketball/target2.png');
        	this.game.load.image('blackLine','assets/basketball/black4x4.png');
        	this.game.load.image('redLine','assets/basketball/red4x4.png');
        	this.game.load.spritesheet('powerbar','assets/basketball/powerbar.png',32,64);

        	this.game.load.image('ballcrowd','assets/basketball/crowd.png');
        	this.game.load.image('bbtut0','assets/basketball/basketballTutorial.png');
        	this.game.load.image('bbtut1','assets/basketball/basketballControls.png');
        	this.game.load.spritesheet('horseSprites','assets/basketball/HORSE.png',64,64);
        	
    },
    
    create: function(){
            lastClickTimestamp=this.game.time.now;
           	shootTimestamp = this.game.time.now;
        	this.game.physics.startSystem(Phaser.Physics.ARCADE);
        	background = this.game.add.sprite(0,0,'background');
        	crowd1 = this.game.add.sprite(-20, 300, 'ballcrowd');
        	crowd2 = this.game.add.sprite(410, 300, 'ballcrowd');
        	//onepointline = game.add.sprite(0, 385, 'onepointline');
        	//twopointline = game.add.sprite(0, 385, 'twopointline');
        	//threepointline = game.add.sprite(0, 385, 'threepointline');
        
            topLine = this.game.add.group();
            topLine.enableBody = true;
            for(var i = 0; i < WIDTH/32; i++){
            	var newLine = topLine.create(i*32,385,'line');
            	newLine.body.immovable = true;
            }
            
            grid = this.game.add.group();
            for(var i = GRID_LEFT; i <= GRID_RIGHT; i+=LINE_SIZE){
               
                for(var j = GRID_TOP; j <= GRID_BOTTOM; j+=LINE_SIZE){
                    if(i%GRID_BOX_SIZE == 0){
                        var newLine = grid.create(i,j, 'blackLine');
                        var columnNumber = ((i-GRID_LEFT)/GRID_BOX_SIZE);
                        if(((i-GRID_LEFT)/GRID_BOX_SIZE)==0){
                                this.game.add.text(i,GRID_BOTTOM+8,"0",{font: '14px Arial', fill: '#000'});
                        }
                        else{
                                this.game.add.text(i,GRID_BOTTOM+8,columnNumber,{font: '14px Arial', fill: '#000'});
                        }
                    }
                    else{
                        if(j%GRID_BOX_SIZE == 0){
                            var newLine = grid.create(i,j,'blackLine');
                            var rowNumber = GRID_Y_SIZE+GRID_Y_SIZE-(j/GRID_BOX_SIZE);
                            if(GRID_Y_SIZE+GRID_Y_SIZE-(j/GRID_BOX_SIZE)==0){
                                this.game.add.text(GRID_LEFT-32,j,"0",{font: '14px Arial', fill: '#000'});
                            }
                            else{
                                this.game.add.text(GRID_LEFT-32,j,rowNumber,{font: '14px Arial', fill: '#000'});
                            }
                            
                        }
                    }
                   
                }
            }
            
        
        
        	basket = this.game.add.sprite(WIDTH/2 -16, HEIGHT/8 *3+5, 'basket');
        	hitBox = this.game.add.group();
        	hitBox.enableBody = true;
        	hitBox.create(WIDTH/2 -TARGET_RADIUS, HEIGHT/8 *3+5,'target');
        	hitBox.visible = false;
        //	target.enableBody = true;
        	
        	player = this.game.add.sprite(100,600,'dude');
        	this.game.physics.arcade.enable(player);
        	player.enableBody = true;
            player.body.bounce.y = 0.2;
            player.body.collideWorldBounds = true;
            player.animations.add('left', [0, 1, 2, 3], 10, true);
            player.animations.add('right', [5, 6, 7, 8], 10, true);
            player.animations.add('up',[4],10,true);
            player.animations.add('down',[4],10,true);
            
            
           
            
            
            balls = this.game.add.group();
            balls.enableBody = true;
            var ball = balls.create(200,200,'ball');
            ball.body.collideWorldBounds=true;
            ball.body.gravity.y = 300;
            ball.body.bounce.y = 0.5;
            ball.body.bounce.x = 0.5;
            ball.animations.add('ballleft', [0,3,2,1], 10, true);
            ball.animations.add('ballright', [0,1,2,3], 10, true);
        
            
            fakeBall = this.game.add.sprite(200,200,'ball');
            fakeBall.visible = false;
            
            powerBar = this.game.add.sprite(0,0,'powerbar');
            
            
            spacebar = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
            wKey = this.game.input.keyboard.addKey(Phaser.Keyboard.W);
            aKey = this.game.input.keyboard.addKey(Phaser.Keyboard.A);
            sKey = this.game.input.keyboard.addKey(Phaser.Keyboard.S);
            dKey = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
            
            scoreDisplay = this.game.add.text(10,40, scoreString + basketballScore, {font: '40px Arial', fill: '#000'});
            powerDisplay = this.game.add.text(10,40, powerString, {font: '12px Arial', fill: '#000'});
            powerString.visible = false;
           // controlDisplay = this.game.add.text(200,40,controlString,{font: '12px Arial', fill: '#000'});
           // locationDisplay = this.game.add.text(500,40,locationString,{font: '12px Arial', fill: '#000'});
           // locationDisplay.visible = false;
            questionDisplay = this.game.add.text(500, 80, questionString,{font: '40px Arial', fill: '#000'});
            wrongspotDisplay = this.game.add.text(500, 100, wrongspotString,{font: '40px Arial', fill: '#000'});
            testShouldScoreDisplay = this.game.add.text(500, 120, wrongspotString,{font: '14px Arial', fill: '#000'});
            
            
            
            
             //initialize music
             this.game.music.stop();
            this.game.music = this.game.add.audio('basketballTheme');
            this.game.music.loop = true;
            this.game.music.play();
            
            tut1Sprite=this.game.add.sprite(0,0,'bbtut1');
            tut0Sprite=this.game.add.sprite(0,0,'bbtut0');
            
            for(var i = 0; i < 5; i++){
                
                horseSpriteArray[i] = this.game.add.sprite(200+80*i,100,'horseSprites');
                horseSpriteArray[i].frame = i*3;
                
                horseSpriteArray[i].animations.add('flash',[i*3, (i*3)+1, (i*3)+2],10,true);
                horseSpriteArray[i].visible = false;
            }
            
    },
    
    update: function(){
            this.updateLocation();
            
            
            if(this.game.input.activePointer.isDown && tutorialStage<2 && this.game.time.now > lastClickTimestamp+500){
                lastClickTimestamp=this.game.time.now;
                if(tutorialStage == 0){
                    tutorialStage = 1;
                    tut0Sprite.visible = false;
                }
                else{
                    tutorialStage = 2;
                    tut1Sprite.visible = false;
                }
            }
            
            this.game.physics.arcade.collide(player,topLine);
        	this.game.physics.arcade.overlap(balls,hitBox,this.scoreFunction,null,this);
        	scoreDisplay.text = scoreString+basketballScore;
        	this.gameWon();
        	
        	this.animatePowerbar();
        	if(holdingBall){
        		balls.visible = false;
        		fakeBall.visible = true;
        		fakeBall.x = player.x;
        		fakeBall.y = player.y + 20;
        	}
        	else if(shootTimestamp + SHOOT_COOLDOWN < this.game.time.now){
        		this.game.physics.arcade.overlap(player,balls,this.pickup,null,this);
        	}
        	
        	
        	if(wKey.isDown){
        		player.body.velocity.y = -200;
        	}
        	else if(sKey.isDown){
        		player.body.velocity.y = 200;
        	}
        	else{
        		player.body.velocity.y = 0;
        	}
        	
        	if(aKey.isDown){
        		player.body.velocity.x = -200;
        		player.animations.play('left');
        	}
        	else if(dKey.isDown){
        		player.body.velocity.x = 200;
        		player.animations.play('right');
        	}
        	else{
        		player.animations.stop();
        		player.body.velocity.x = 0;
        		player.frame = 4;
        	}
        	
        	if(holdingBall && spacebar.isDown){
        		
        		if(poweringUp == false && poweringDown == false){ //initial case
        		    poweringUp = true;
        		}
        		
        		if(poweringUp == true){
        		    power+=1;
        		    if(power>MAX_POWER){
        		        poweringDown = true;
        		        poweringUp = false;
        			    power=MAX_POWER;
        		    }
        		}
        		
        		else if(poweringDown == true){
        		    power-=1;
        		    if(power<0){
        		        poweringUp = true;
        		        poweringDown = false;
        		        power=0;
        		    }
        		}
        		
        			
        	//	powerDisplay.x = player.x;
        	//	powerDisplay.y = player.y+50;
        	//	powerDisplay.visible = true;
        	//	powerDisplay.text = powerString + power;
        	}
        	else if(spacebar.isUp && holdingBall && (poweringUp || poweringDown)){
        		poweringUp = false;
        		poweringDown = false;
        		this.shoot(power);
        	}
        	else{
        		powerDisplay.visible = false;
        		poweringUp = false;
        		poweringDown = false;
        		power = 0;
        	}
        	if(haveShot==true && holdingBall){
                this.generateQuestion();
            }
            
           
    },
    
    shoot: function(){
            haveShot = true;
            haveScored = false;
        	shootTimestamp = this.game.time.now;
        	holdingBall = false;
        	balls.visible = true;
        	fakeBall.visible = false;
        	
        	var x= this.game.input.activePointer.x; 
        	var y= this.game.input.activePointer.y;
        	
        	var ball = balls.create(player.x, player.y,'ball');
            ball.body.collideWorldBounds=true;
            ball.body.gravity.y = 300;
            ball.body.bounce.y = 0.5;
            ball.body.bounce.x = 0.5;
            ball.animations.add('spin',[0,1,2,3],10,true);
            ball.animations.play('spin');
        	
        	var yDist = player.y - y;
        	var xDist;
        	ball.animations.play('ballleft');
        	
        	if(power > MAX_POWER){
        		power = MAX_POWER;
        	}
        	var strength = power*10;
        	
        	if(x < player.x){ //to left of player
                        xDist = player.x - x;
                	    var theta = (Math.atan(yDist/xDist));
                	    ball.body.velocity.y = - (strength * Math.sin(theta));
                	    ball.body.velocity.x = - (strength * Math.cos(theta));
                	    ball.animations.play('ballleft');
            }
            else if(x> player.x){ //to right of player
                        xDist = x - player.x;
                	    var theta = (Math.atan(yDist/xDist));
                	    ball.body.velocity.y = - (strength * Math.sin(theta));
                	    ball.body.velocity.x = + (strength * Math.cos(theta));
                        ball.animations.play('ballright');
                
            }
            else{
                        ball.body.velocity.y = -strength;
            }
            
            this.game.ballshotsTaken++;
            
    },
    
    pickup: function(player, ball){
            ball.kill();
	        holdingBall = true;
    },
    
    generateQuestion: function(){
              questionXLocation = Math.floor(Math.random() * GRID_X_SIZE)+1;
              questionYLocation = Math.floor(Math.random() * GRID_Y_SIZE)+1;
              questionXY= questionXLocation + ", " + (Math.abs(questionYLocation - 6));
              questionDisplay.visible = true;
              questionDisplay.text = "Score from " + questionXY + "!";
              haveShot = false;
              
    }, 
    scoreFunction: function(ball, target){
        
        if((playerGridXLocation == questionXLocation) && (playerGridYLocation == questionYLocation)){
            shouldScore = true;
            testShouldScoreDisplay.visible = true;
        }
        else{
            shouldScore = false;
        }
           if((ball.body.velocity.y > 0 || realismMode == false) && !haveScored){  //can only score if ball is going down
                if(ball.body.velocity.y < 0){
                   ball.body.velocity.y = 0;
                }
                if(shouldScore == true) {//doing it this way so the ball always goes through the basket correctly
        		    basketballScore++;        //but points only granted if in right location
        		    wrongspotDisplay.visible = false;
                }
        		else{
        		    wrongspotDisplay.visible = true;
        		    wrongspotDisplay.text = "Wrong spot";
        		    this.game.ballshotsMissed++;
        		    //tell them theyre in the wrong spot
        		}
        		haveScored = true;
        		ball.body.x = target.body.x + TARGET_RADIUS;
        		ball.body.velocity.x = 0;
        	}
        	else if(!haveScored){
        		ball.body.y += 10;
        		ball.body.velocity.y = 50;
        		this.game.ballshotsMissed++;
        	}
        	
        for(var i = 0; i < basketballScore; i++){
            horseSpriteArray[i].visible = true;
        }
        for(var i = basketballScore; i < 5; i++){
            horseSpriteArray[i].visible = false;
        }
    },
    
    updateLocation: function(){
        if(player.x > GRID_LEFT && player.x < GRID_RIGHT && player.y > GRID_TOP && player.y < GRID_BOTTOM){
            playerOnGrid = true;
            var xLocOnGrid = player.x - GRID_LEFT;
            var yLocOnGrid = player.y - GRID_TOP;
            
            playerGridXLocation = Math.floor(xLocOnGrid/GRID_BOX_SIZE) +1;
            playerGridYLocation = Math.floor(yLocOnGrid/GRID_BOX_SIZE) +1;
           // locationDisplay.visible = true;
           // locationDisplay.text = playerGridXLocation + ", " + (Math.abs(playerGridYLocation - 6));
        }
        else{
            playerGridXLocation = -1;
            playerGridYLocation = -1;
            playerOnGrid = false;
          //  locationDisplay.visible = false;
        }
        
    },
    
    gameWon: function(){
        if(basketballScore >= SCORETOWIN){
            this.game.basketballScore = SCORETOWIN;
            this.game.state.start("BasketballEnding");
        }
    },
    
    animatePowerbar: function(){
        if(poweringUp==true || poweringDown ==true){
            powerBar.visible = true;
            powerBar.x = player.x+32;
            powerBar.y = player.y-32;
            
            var N_POWERBAR_SLIDES = 17;
            var slideNumber = Math.floor(power/MAX_POWER * N_POWERBAR_SLIDES);
            powerBar.frame = slideNumber;
        }
        else{
            powerBar.visible = false;
        }
                
    }
    
}