var conveyorGame = function(game){
	conveyorGame.game = game;
	
	//groups
	this.tires;
	this.boxes;
	this.boxClones;
    this.resetLines;
	
	//question vars
	this.randomNums = [];
	this.randomAnswersArray1 = [];
    this.randomAnswersArray2 = [];
    this.randomAnswersArray3 = [];
	this.actualAnswer;
	this.displayedAnswers = [];
	this.questionText;
	this.questionString;
    
	//statics
	this.BOX_VELOCITY=100; // Velocity of boxes in any direciton
	this.DIFFICULTY_LEVEL; // Managing difficulty facotrs  
	this.BOX_GAP = 200;
	this.SCORE_TO_WIN = 200; // The score needed to win this minigame 
	this.BOX_SCALEXY = 0.2; // The scale of the box since the real pic is giant
	this.CURR_SCORE_FACTOR = 20; // Score sub'd or added on each iteration of problem
	this.NEXT_STATE = "ConveyorEnding"
    this.TIME_FOR_DIFFICULTY_1 = 8; // Amount of time per problem on difficulty 1
    this.TIME_FOR_DIFFICULTY_2 = 20; // Amount of time per problem on difficulty 2
    this.TIME_FOR_DIFFICULTY_3 = 15; // Amount of time per problem on difficulty 3
	
	//other
	this.score;
	this.scoreText;
    this.alienHappyFace;
    this.alienSadFace;
    this.scoreName;
    this.timer;
    this.timerName;
    this.timerText;
    this.hintName;
    this.hint;
    this.conveyorHolding;
    this.hintActive;

	//variable part drawing
	this.nPartSpritesheets = 4;
	this.activeSpritesheet = 0;
	this.spritesheetArray = ['part0','part1','part2','part3'];
	this.nSlidesPerSheet = 10;
	
	this.tutorialImage;
}

conveyorGame.prototype =  {
    
    
	preload: function(){
   	 
    	this.game.load.image('tiretexture', 'assets/conveyorgame/tiretexture.jpg'); //a tire texture (unused currently)
    	this.game.load.image('rectangle', 'assets/conveyorgame/rectangle.jpg'); //a simple rectangle for conveyor belt
    	this.game.load.image('y_rectangle', 'assets/conveyorgame/shoot.jpg'); //the rectangle that acts as a shoot
    	this.game.load.image('ufo', 'assets/conveyorgame/ufo.png'); //the ufo
    	this.game.load.image('box', 'assets/conveyorgame/box.png'); //the box on conveyor
    	this.game.load.image('topLine','assets/conveyorgame/topLine.png'); //the topline used to reset box positions
    	this.game.load.spritesheet('part0','assets/conveyorgame/battery_64_64.png',64,64);
    	this.game.load.spritesheet('part1','assets/conveyorgame/button_64_64.png',64,64);
    	this.game.load.spritesheet('part2','assets/conveyorgame/chip_64_64.png',64,64);
    	this.game.load.spritesheet('part3','assets/conveyorgame/spring_64_64.png',64,64);
    	this.game.load.image('convTut','assets/conveyorgame/conveyorTutorial.png');
        this.game.load.image('alienHappyFace','assets/conveyorgame/alien_joyeous.png');
        this.game.load.image('alienSadFace','assets/conveyorgame/alien_sad.png');
        this.game.load.image('great','assets/conveyorgame/great.png');
        this.game.load.image('oops','assets/conveyorgame/oops.png');
	},
    
	create: function(){

        if (this.game.freePlayMode==true) {
            this.SCORE_TO_WIN = 9999999999; // 'Never end'
        }

        this.CONVEYOR_1_BOX_Y = this.game.height-480;
        this.CONVEYOR_2_BOX_Y = this.game.height-300;
        this.CONVEYOR_3_BOX_Y = this.game.height-125;

        //Music
        this.game.music = this.game.add.audio('conveyorTheme');
        this.game.music.loop = true;
        this.game.music.play();

    	//currently hard-coded diff level
    	this.DIFFICULTY_LEVEL = 1;
   	 
   	 	//get physics going
    	this.game.physics.startSystem(Phaser.Physics.ARCADE);
    	
    	//option for setting background color
    	this.game.stage.backgroundColor = '#000000';
   	 
    	//setup the score
    	this.score = 0;
    	var scoreStyle = { font: "30px Arial", fill: "#ffffff", align: "center" };
        this.scoreName = this.game.add.text(20, 20, "Score: ", scoreStyle); 
        this.scoreText = this.game.add.text(115, 20, "0", scoreStyle);

        //Question
        var questionStyle = { font: "42px Arial", fill: "#ffffff", align: "center" };

        //Prompt Solve:
        var promptStyle = { font: "42px Arial", fill: "#7CFC00", align: "center" };
        this.promptText = this.game.add.text(this.game.world.centerX+50, this.game.world.centerY-180, "Solve:", promptStyle);

        //Timer:
        var timerStyle = { font: "20px Arial", fill: "#DB2929", align: "center" };
        this.timer = 10;
        this.gameTimer = this.game.time.create(false);
        this.gameTimer.loop(1000, this.updateCounter, this);
        
    	this.timerName = this.game.add.text(20, 50, "Hint Timer:", timerStyle);
        this.timerText = this.game.add.text(120, 50, "", timerStyle);

        //Hint:
        var hintStyle = { font: "30px Arial", fill: "#00CED1", align: "center" };
        
        this.hintName = this.game.add.text(510, 180, "Hint:", hintStyle);
        this.hintName.visible = false;
        this.hintText = this.game.add.text(585, 180, "", hintStyle);
        this.hintText.visible = false;

    	//add tire/conveyer texture group
    	this.tires = this.game.add.group();
    	this.tires.enableBody = true;
   	 
    	
    	//used to add tweens to the 'box' that's chosen
    	this.boxClones = this.game.add.group();
    	this.boxClones.enableBody = true;
    
        //the boxes on the belt
    	this.boxes = this.game.add.group();
    	this.boxes.enableBody = true;

        //make the lines that reset boxes and the shoots
        this.makeResetLinesAndShoots();
    
        //instantiate question text
        this.questionText = this.game.add.text((this.game.world.centerX)+200, (this.game.world.centerY-180), " ", questionStyle);

    	//make a new row of conveyor belt
    	this.addRowOfTires();

        this.alienSadFace = this.game.add.sprite(475, 20, 'alienSadFace');
        this.alienSadFace.scale.setTo(.5,.5);
        this.alienSadFace.visible = false;

        this.alienHappyFace = this.game.add.sprite(475, 20, 'alienHappyFace');
        this.alienHappyFace.scale.setTo(.5,.5);
        this.alienHappyFace.visible = false;

        this.great = this.game.add.sprite(610, 20, 'great');
        this.great.scale.setTo(.5,.5);
        this.great.visible = false;

        this.oops = this.game.add.sprite(610, 70, 'oops');
        this.oops.scale.setTo(.5,.5);
        this.oops.visible = false;

        //generate a new problem
    	this.newProblem();

        //tutorial image, added last so it's over everything
        this.tutorialImage = this.game.add.sprite(0,0,'convTut');
        this.spacebar = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

	},

    updateCounter: function() {
        if (this.timer > 0) {
            this.timer--;    
            this.timerText.setText(this.timer);
        }
    },
    
	update: function() {

		if(this.game.input.activePointer.isDown){
   		 	this.tutorialImage.visible = false;
   		 }

        if (this.timer==0) {
            this.hintName.visible = true;
            this.hintText.setText("Conveyor " + this.conveyorHolding.toString());
            this.hintText.visible = true;
            this.hintActive = true;
        }
    	 //check for winner
    	 this.isGameWon();
    	 
    	 //the collide that makes boxes go back down to where they came FROM
   		 this.game.physics.arcade.overlap(this.boxes,this.resetLines,this.resetBox,null,this);
	},

    makeResetLinesAndShoots: function() {
        //This is the line at the side of the conveyor used to move the boxes back to the bottom.
        this.resetLines = this.game.add.group();
        this.resetLines.enableBody = true;
        var line1 = this.resetLines.create(this.game.width+50,this.game.world.centerY-120,'topLine');
        var line2 = this.resetLines.create(-50,this.game.world.centerY+55, 'topLine');
        var line3 = this.resetLines.create(this.game.width+50,this.game.height-170,'topLine');

        //add the shoot things, don't need a group because no collision on these
        var shoot1 = this.game.add.sprite(-20, this.game.world.centerY-140, 'y_rectangle');
        shoot1.scale.setTo(.5,.6);
        var shoot2 = this.game.add.sprite(this.game.width-45, this.game.world.centerY+35, 'y_rectangle');
        shoot2.scale.setTo(.5,.6);
        var shoot3 = this.game.add.sprite(-20, this.game.world.centerY+210, 'y_rectangle');
        shoot3.scale.setTo(.5,.6);

    },
    
	addOneTire: function(x, y) {
    	// Get the first dead pipe of our group
    	var tire = this.tires.create(x,y,'rectangle');
        tire.scale.setTo(1, .5);
    
    	// Set the new position of the texture
    	tire.reset(x, y);
    	// Add velocity to the texture to make it go up
    	// tire.body.velocity.y = this.TIRE_VELOCITY;
    
    	// Kill the tire texture when it's no longer visible
    	tire.checkWorldBounds = true;
    	tire.outOfBoundsKill = true;
	},
    
	addRowOfTires: function() {
    	// Add the 6 tires
	for (var i = 0; i < 5; i++) {
    	this.addOneTire(i * 200, this.game.height - 175);
        this.addOneTire(i * 200, this.game.height - 350);   
        this.addOneTire(i * 200, this.game.height - 525);        
    }    

	},
    
	newProblem: function() {
        if (this.DIFFICULTY_LEVEL==1) {
            this.timer = this.TIME_FOR_DIFFICULTY_1
        } else if (this.DIFFICULTY_LEVEL==2) {
            this.timer = this.TIME_FOR_DIFFICULTY_2
        } else if (this.DIFFICULTY_LEVEL==3) {
            this.timer = this.TIME_FOR_DIFFICULTY_3
        }

        this.gameTimer.start();

        this.hintName.visible = false;
        this.hintText.visible = false;
        this.hintActive = false;

        this.game.numberOfConveyorQuestions++;
        //handle images
        this.alienSadFace.visible = true;
        this.alienHappyFace.visible = false;
        this.great.visible = false;
        this.oops.visible = false;

    	//remove all our boxes
    	if (this.boxes.length > 0) {
        	for (var i=0; i < this.boxes.length; i ++) {
            	this.boxes.getAt(i).kill();
        	}
        }

    	//nullify all our variables
    	this.randomNums.length = 0;
    	this.randomAnswersArray1.length = 0;
        this.randomAnswersArray3.length = 0;
        this.randomAnswersArray3.length = 0;
    	this.displayedAnswers.length = 0;
    
    	this.actualAnswer = null;
    	//generate random problem and answers, and get real answer
    	this.addProblem();
    
        //now that we have our question, answer, and dummy answers, make boxes for the answers
     	this.generateBoxes(this.randomAnswersArray1, 1);
        this.generateBoxes(this.randomAnswersArray1, 2);
        this.generateBoxes(this.randomAnswersArray1, 3);

	},
    
	addProblem: function() {
    	//add a ufo
        this.brokenUfo = this.game.add.sprite(this.game.world.centerX-325,-200, 'ufo'); //make a new ufo
        this.brokenUfo.scale.setTo(.7, .7); //make it smaller 
        this.fly_in_tween = this.game.add.tween(this.brokenUfo).to({ y: this.game.world.centerY-350 }, 2000, Phaser.Easing.Linear.None, true);


    	switch(this.DIFFICULTY_LEVEL) { //choose a range based on difficulty
        	case(3):
            	var HIGH = 40;
            	var LOW = 30;
            	break;
        	case(2):
            	var HIGH = 30;
            	var LOW = 20;
            	break;
        	case(1):
            	var HIGH = 20;
            	var LOW = 10;
    
    	}
    
    	//create our question numbers
    	for (var i = 0; i < 2; i ++) {
    	   //create a random number from -LOW to +LOW
    	   var randomNumber = Math.floor(Math.random() * HIGH) - LOW;
    	   this.randomNums[i] = randomNumber;
    	}

        this.deriveActualAnswer();
    
        
    	
    	console.log("CHEAT: Answer is : " + this.actualAnswer);

        //an array of our random answer arrays
        this.arrayOfArrays=[this.randomAnswersArray1, this.randomAnswersArray2, this.randomAnswersArray3];
        //puts the answer in one of our random answer arrays
        var indexOfArrayContainingAnswer = Math.floor(Math.random()*3);
        
        if (indexOfArrayContainingAnswer==0) {
            this.randomAnswersArray1[0] = this.actualAnswer;
            this.conveyorHolding = 1;
            console.log("Conveyor 1 Got The Answer" +  (indexOfArrayContainingAnswer+1));
        }
        if (indexOfArrayContainingAnswer==1) {
            this.randomAnswersArray2[0] = this.actualAnswer;
            this.conveyorHolding = 2;
            console.log("Conveyor 2 Got The Answer" +  (indexOfArrayContainingAnswer+1));
        }
        if (indexOfArrayContainingAnswer==2) {
            this.randomAnswersArray3[0] = this.actualAnswer;
            this.conveyorHolding = 3;
            console.log("Conveyor 3 Got The Answer" +  (indexOfArrayContainingAnswer+1));
        }

    	//now generate fake answers
    	for (var i = 1; i < 10; i ++) {
            var randomAnswer1 = Math.floor(Math.random() * HIGH) - LOW;
            var randomAnswer2 = Math.floor(Math.random() * HIGH) - LOW;
            var randomAnswer3 = Math.floor(Math.random() * HIGH) - LOW;

            //a lazy, lazy way of making sure we don't put our 'answer' in these random answer arrays.
            //if we do, the check to see if they are right doesn't work properly (some memory assignment issue),
            //idk its 4:30am
            this.checkForDuplicatesAndInput(randomAnswer1, this.randomAnswersArray1, i);
            this.checkForDuplicatesAndInput(randomAnswer2, this.randomAnswersArray2, i);
            this.checkForDuplicatesAndInput(randomAnswer3, this.randomAnswersArray3, i);
    	}
	},

    deriveActualAnswer: function() {
        //choose if we're going to do addition or subtraction
        var opChoice = Math.floor(Math.random()*2);
        console.log("OPCHOICE ", opChoice);
        var operator;
        if (opChoice == 0) {operator="+";}
        else {operator="-";}
        //our question in string format
        this.questionString = this.randomNums[0] + " " + operator + " " + this.randomNums[1];
        this.questionText.text = this.questionString; //makes the display text our question
        // questionText.text = qString;
        //our answer
        if (operator=="+") {this.actualAnswer = eval(this.randomNums[0] + this.randomNums[1]);}
        else {this.actualAnswer = eval(this.randomNums[0] - this.randomNums[1]);}
        if (this.actualAnswer == 0 || this.actualAnswer==null) {
            console.log("TRIED AGAIN * * * *");
            this.deriveActualAnswer();
        } //no zeros allowed
    },

    checkForDuplicatesAndInput: function(answer, array, ind) {
        if (answer!=this.actualAnswer) {
            array[ind] = answer;
        } else {array[ind] = answer + 1;}
    },

	generateBoxes: function(arrayOfAnswers, conveyorNumber) {
    	//find a new answer
    	arrayOfAnswers = this.fischerYatesShuffle(arrayOfAnswers);    //shuffle the random answers

    	this.activeSpritesheet = Math.floor(Math.random() * this.nPartSpritesheets);
    	for(var i = 0; i < arrayOfAnswers.length; i++){  //iterate over the answers, for each spawn a new box
 	         

           if (conveyorNumber==1) {   
    	   //Create the box sprite and attach the string to it
        	var box = this.boxes.create(this.BOX_GAP*i*-1, this.CONVEYOR_1_BOX_Y, this.spritesheetArray[this.activeSpritesheet]);
            box.frame = Math.floor(Math.random() * this.nSlidesPerSheet);
            box.body.velocity.x = this.BOX_VELOCITY; //make it move
            box.value = this.randomAnswersArray1[i]; //give it a value to hold onto
        	}

            if (conveyorNumber==2) {   
           //Create the box sprite and attach the string to it
            var box = this.boxes.create(this.game.width+this.BOX_GAP*i, this.CONVEYOR_2_BOX_Y, this.spritesheetArray[this.activeSpritesheet]);
            box.frame = Math.floor(Math.random() * this.nSlidesPerSheet);
            box.body.velocity.x = this.BOX_VELOCITY*-1; //make it move
            box.value = this.randomAnswersArray2[i]; //give it a value to hold onto
            }

            if (conveyorNumber==3) {   
           //Create the box sprite and attach the string to it
            var box = this.boxes.create(this.BOX_GAP*i*-1, this.CONVEYOR_3_BOX_Y, this.spritesheetArray[this.activeSpritesheet]);
            box.frame = Math.floor(Math.random() * this.nSlidesPerSheet);
            box.body.velocity.x = this.BOX_VELOCITY; //make it move
            box.value = this.randomAnswersArray3[i]; //give it a value to hold onto
            }

        	box.inputEnabled = true; //let us click it
        	box.events.onInputDown.add(this.listener, this); //tell it what to do when clicked
        	var display;
       	 	if(box.value == null || box.value ==0){
                console.log("we have a zero");
       	 		display = this.game.add.text(-32,0,"0", {font: '32px Arial', fill: '#000' }); //make text out of the value
       	 	}
       	 	else{
       	 		display = this.game.add.text(-32,0,box.value, {font: '32px Arial', fill: '#000' }); //make text out of the value
       	 	}
        	
        	box.addChild(display); //attach the text
    	}
      	 
   	 
	},
    
	//listens for a click on an answer and acts
	listener: function(sprite, pointer) {
    	if (sprite.value==this.actualAnswer) {

            //handle images
            this.alienHappyFace.visible = true;
            this.great.visible = true;
            this.oops.visible = false;

    	    //here we make a clone so that we can kill the old box but still make a fancy tween
        	var clone = this.boxClones.create(sprite.x, sprite.y, 'box');
        	clone.scale.setTo(this.BOX_SCALEXY,this.BOX_SCALEXY); //make it smaller
        	clone.body.velocity.y = this.BOX_VELOCITY; //make it move
        	clone.value = sprite.value;
        	var cloneDisplay;
        	if(sprite.value == 0){
        		cloneDisplay = this.game.add.text(150,175, "0", {font: '200px Arial', fill: '#000' }); //make text out of the value
        	}
        	else{
        		cloneDisplay = this.game.add.text(150,175, clone.value, {font: '200px Arial', fill: '#000' }); //make text out of the value
        	}
            clone.addChild(cloneDisplay); //attach the text
        	clone.checkWorldBounds = true;
        	clone.outOfBoundsKill = true;
        	this.game.physics.arcade.moveToObject(clone, this.scoreText, 400); //do the actual move
        	//increment score
        	this.crankItUp(); //turns velocity up and increases score factor if applicable
        	
            this.fly_out_tween = this.game.add.tween(this.brokenUfo).to({ y: -2000 }, 1000, Phaser.Easing.Linear.None, true).onComplete.add(this.newProblem, this); //make it fly in

    	} else {
    	    if (this.score > 0) {
                this.face_tween = this.game.add.tween(this.alienSadFace).to( {alpha: 1}, 2000, Phaser.Easing.Linear.None, true, 1, 2000, true);
    	        this.crankitDown(); //turns velocity down and decreases score factor if applicable
                this.oops.visible=true;
                this.game.numberOfConveyorMissed++;
    	    }
    	}
        sprite.kill();
    	
	},
    
	fischerYatesShuffle: function(array){
      	var currentIndex = array.length, temporaryValue, randomIndex ;

      	// While there remain elements to shuffle...
      	while (0 !== currentIndex) {
   	 
        	// Pick a remaining element...
        	randomIndex = Math.floor(Math.random() * currentIndex);
        	currentIndex -= 1;
   	 
        	// And swap it with the current element.
        	temporaryValue = array[currentIndex];
        	array[currentIndex] = array[randomIndex];
        	array[randomIndex] = temporaryValue;
      	}
   	 
      	return array;
	},
    
	resetBox: function(box, line){
        if (box.body.y==this.CONVEYOR_2_BOX_Y) {
            box.body.x = this.game.width + this.BOX_GAP*6;   
        }
        else {
            box.body.x = this.BOX_GAP*-6;
        }
    	
	},
	
	modifyScoreFactor: function() {
		if (this.BOX_VELOCITY > 350) {
            this.CURR_SCORE_FACTOR = 150;
            this.DIFFICULTY_LEVEL = 3;
        } else if (this.BOX_VELOCITY > 300) {
			this.CURR_SCORE_FACTOR = 100;
            this.DIFFICULTY_LEVEL = 2;
		} else if (this.BOX_VELOCITY > 250) {
			this.CURR_SCORE_FACTOR = 50;
            this.DIFFICULTY_LEVEL = 1;
		} else {
			this.CURR_SCORE_FACTOR = 20;
            this.DIFFICULTY_LEVEL = 1;
		}
	},

	crankItUp: function() {
		this.BOX_VELOCITY+=20;
		this.modifyScoreFactor();
        var toBeAdded = this.CURR_SCORE_FACTOR;
        if (this.hintActive==true) {
            this.score += this.CURR_SCORE_FACTOR - this.CURR_SCORE_FACTOR/2; //take off some pts
        } else {
            this.score += this.CURR_SCORE_FACTOR; //adds current score factor    
        }
        this.scoreText.text = this.score;
	},
	
	crankitDown: function() {
		this.BOX_VELOCITY-=20;
		this.modifyScoreFactor();
        this.score -= this.CURR_SCORE_FACTOR; //subtracts current score factor
        this.scoreText.text = this.score;
	},
	
	
	isGameWon: function() {
	    if (this.score>=this.SCORE_TO_WIN) {
            this.game.conveyorScore = this.score;
	        this.game.state.start(this.NEXT_STATE);
	    }
	}
};
